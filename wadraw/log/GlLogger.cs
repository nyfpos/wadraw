﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace wadraw
{
    public class GlLogger : Singleton<GlLogger>
    {
        private Action<object[]> _logger;
        public string filePath = @"wa_gl_logg.log";
        public GlLogger()
        {
            _logger = null;
        }

        private void _log(string message)
        {
            using (StreamWriter streamWriter = new StreamWriter(filePath, true))
            {
                streamWriter.WriteLine(message);
                streamWriter.Close();
            }           
        }

        public void BindLogInfo(Action<object[]> logger)
        {
            _logger = logger;
        }

        public void LogInfo(params object[] args)
        {
            if (_logger != null)
            {
                _logger(args);
            }
        }

        private string _tostr(object[] args)
        {
            string rtn = "", glue ="";
            for (int i = 0; i < args.Length; i++)
            {
                rtn = rtn + glue + args[i].ToString();
                glue = " ";
            }
            return rtn;
        }

        public void Debug(params object[] args)
        {
            _log(String.Format("[{0}] [Debug] {1}", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), _tostr(args)));
        }

        public void Info(params object[] args)
        {
           _log(  String.Format("[{0}] [Info] {1}", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), _tostr(args)));
        }

        public void Warn(params object[] args)
        {
            _log( String.Format("[{0}] [Warn] {1}", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), _tostr(args)));
        }

        public void Error(params object[] args)
        {
            _log( String.Format("[{0}] [Error] {1}", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), _tostr(args)));
        }

        public void Fatal(params object[] args)
        {
            _log( String.Format("[{0}] [Fatal] {1}", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), _tostr(args)));
        }
    }
}
