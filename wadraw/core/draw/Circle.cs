﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace wadraw
{
    public class Circle :DrawObject2D
    {
        public Circle(float size = 30.0f):base()
        {            
            MeshDim = 3;
            _CreateCircle();
            modelSize = size;
            float halfSize = size * 0.5f;
            _pivotLeftTop = new Vector3(-halfSize, halfSize, 0);
            _pivotCenter = new Vector3(0, 0, 0);
            _pivotCenterBottom = new Vector3(0, -halfSize, 0);
            //the circle default pivot is center point.
            pivotType = PivotType.Center;
        }

        public Circle(int numTriangles, float size = 30.0f):base()
        {            
            MeshDim = 3;
            _CreateCircle(numTriangles);
            modelSize = size;
            float halfSize = size * 0.5f;
            _pivotLeftTop = new Vector3(-halfSize, halfSize, 0);
            _pivotCenter = new Vector3(0, 0, 0);
            _pivotCenterBottom = new Vector3(0, -halfSize, 0);
            //the circle default pivot is center point.
            pivotType = PivotType.Center;
        }

        private void _CreateCircle(int numTriangles = 40)
        {
            int numPoints = numTriangles + 1;
            mesh.color = new byte[numPoints * ColorDim];
            mesh.position = new float[numPoints*MeshDim];
            indices = new uint[numPoints];            
            float thetaUnit = (float)((2.0f * Math.PI) / numTriangles);
            float theta = 0.0f;
            float r = 0.5f;
            for (int i = 0; i < numPoints; i++)
            {               
                theta = (i - 1) * thetaUnit;            
                mesh.position[3*i +0] = (float)(r*Math.Cos(theta));
                mesh.position[3*i +1] = (float)(r*Math.Sin(theta));
                mesh.position[3*i +2] = 0.0f;
                indices[i] = (uint)i;               
            }
            color = 0xFFFFFF;
        }

        internal override void render(float deltaTime, bool hasTexture = false)
        {
            defaultRender(PrimitiveType.Polygon);
        }

        public void reset(float centerX, float centerY, float radius)
        {
            reset(new Point(centerX, centerY), radius);
        }

        public void reset(Point center, float radius)
        {
            float x = center.x;
            float y = center.y;
            float wh = 2.0f * radius;
            reset(x, y, x + wh, y + wh);
        }
    }
}
