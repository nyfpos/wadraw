﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace wadraw
{
    class TextRender : IDisposable
    {
        Bitmap _bmp;
        Graphics _gfx;
        int _texture;
        Rectangle _dirtyRegion;
        bool _disposed;

        // Constructs a new instance.
        public TextRender(int width, int height)
        {
            if (width <= 0)
                throw new ArgumentOutOfRangeException("width");
            if (height <= 0)
                throw new ArgumentOutOfRangeException("height");
            if (GraphicsContext.CurrentContext == null)
                throw new InvalidOperationException("No GraphicsContext is current on the calling thread.");

            _bmp = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            _gfx = Graphics.FromImage(_bmp);
            _gfx.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            _disposed = false;
            _texture = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, _texture);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);
        }

        public int Width
        {
            get
            {
                return _bmp.Width;
            }
        }

        public int Height
        {
            get
            {
                return _bmp.Height;
            }
        }

        // Clears the backing store to the specified color.
        public void Clear(Color color)
        {
            _gfx.Clear(color);
            _dirtyRegion = new Rectangle(0, 0, _bmp.Width, _bmp.Height);
        }

        // Draws the specified string to the backing store.
        // @point: The location of the text on the backing store, in 2d pixel coordinates.
        // The origin (0, 0) lies at the top-left corner of the backing store.
        public void DrawString(string text, Font font, Brush brush, PointF point)
        {
            _gfx.DrawString(text, font, brush, point);
            SizeF size = _gfx.MeasureString(text, font);
            _dirtyRegion = Rectangle.Round(RectangleF.Union(_dirtyRegion, new RectangleF(point, size)));
            _dirtyRegion = Rectangle.Intersect(_dirtyRegion, new Rectangle(0, 0, _bmp.Width, _bmp.Height));
        }

        public int Texture
        {
            get
            {
                UploadBitmap();
                return _texture;
            }
        }

        // Uploads the dirty regions of the backing store to the OpenGL texture.
        void UploadBitmap()
        {
            if (_dirtyRegion != Rectangle.Empty)
            {
                System.Drawing.Imaging.BitmapData data = _bmp.LockBits(_dirtyRegion,
                    System.Drawing.Imaging.ImageLockMode.ReadOnly,
                    System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                GL.BindTexture(TextureTarget.Texture2D, _texture);
                GL.TexSubImage2D(TextureTarget.Texture2D, 0,
                    _dirtyRegion.X, _dirtyRegion.Y, _dirtyRegion.Width, _dirtyRegion.Height,
                    PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);

                _bmp.UnlockBits(data);

                _dirtyRegion = Rectangle.Empty;
            }
        }

        void Dispose(bool manual)
        {
            if (!_disposed)
            {
                if (manual)
                {
                    _bmp.Dispose();
                    _gfx.Dispose();
                    if (GraphicsContext.CurrentContext != null)
                        GL.DeleteTexture(_texture);
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~TextRender()
        {
            Console.WriteLine("[Warning] Resource leaked: {0}.", typeof(TextRender));
        }
    }


    public class DrawText : Quad
    {
        private bool _disposed;
        private int _size;
        private Font _currentFont;
        private FontFamily _currentFamily;
        private uint _fontColor;
        private uint _backgroundColor;
        private string _text;

        TextRender _render;

        private bool _beDirty;

        public DrawText() : base()
        {
            base.width = 30.0f;
            base.height = 30.0f;
            _size =24;
            _currentFamily = FontFamily.GenericSerif;
            _currentFont = new Font(_currentFamily, _size);
            _text = "";
            _render = null;

            /*
             mesh.position = new float[]
            {
                -0.5f, 0.5f, 0.0f,
                0.5f, 0.5f, 0.0f,
                0.5f, -0.5f, 0.0f,
                -0.5f, -0.5f, 0.0f
            };
             */

            _beDirty = true;

            mesh.texCoords = new float[]
            {
                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f,
                0.0f, 1.0f
            };
        }

        public int fontSize
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
                _currentFont = new Font(_currentFamily, _size);
                _beDirty = true;
            }
        }

        public uint fontColor
        {
            get
            {
                return _fontColor;
            }
            set
            {
                _fontColor = value;
                _beDirty = true;
            }
        }

        public uint backgroundColor
        {
            get
            {
                return _backgroundColor;
            }
            set
            {
                _backgroundColor = value;
                _beDirty = true;
            }
        }

        public FontFamily fontFamily
        {
            get
            {
                return _currentFamily;
            }
            set
            {
                _currentFamily = value;
                _currentFont = new Font(_currentFamily, _size);
                _beDirty = true;
            }
        }

        public string text
        {
            get
            {
                return _text;
            }
            set
            {
                _text = value;
                _beDirty = true;
            }
        }

        public override DrawObject2D Clone()
        {
            DrawText dest = (DrawText)base.Clone();

            dest.text = this.text;
            dest.fontSize = this.fontSize;
            dest.fontColor = this.fontColor;
            dest.backgroundColor = this.backgroundColor;
            dest.fontFamily = this.fontFamily;

            return dest;
        }

        public override JObject toJObject()
        {
            dynamic jsonObject = base.toJObject();
            jsonObject.text = this.text;
            jsonObject.fontSize = this.fontSize;
            jsonObject.fontColor = this.fontColor;
            jsonObject.backgroundColor = this.backgroundColor;
            jsonObject.fontFamily = this._currentFont.Name;
            return jsonObject;
        }

        public override void fromJObject(JObject jsonObject)
        {
            base.fromJObject(jsonObject);
            if (jsonObject["text"] != null)
                this.text = (string)jsonObject["text"];
            if (jsonObject["fontSize"] != null)
                this.fontSize = (int)jsonObject["fontSize"];
            if (jsonObject["fontColor"] != null)
                this.fontColor = (uint)jsonObject["fontColor"];
            if (jsonObject["backgroundColor"] != null)
                this.backgroundColor = (uint)jsonObject["backgroundColor"];
            if (jsonObject["fontFamily"] != null)
                this.fontFamily = new FontFamily((string)jsonObject["fontFamily"]);            
        }

        private void _updateRender(SizeF size)
        {
            if (_render == null || _render.Width < size.Width || _render.Height < size.Height)
            {
                if (_render != null)
                    _render.Dispose();

                int w = (int)width;
                int h = (int)height;
                if (w > canvasInfo.width)
                    w = canvasInfo.width;
                if (h > canvasInfo.height)
                    h = canvasInfo.height;
                _render = new TextRender(w, h);
            }
//          GlLogger.Instance.LogInfo(string.Format("render.width:{0} size.width:{1}", _render.Width, size.Width));
        }

        internal override void render(float deltaTime, bool hasTexture = false)
        {
            fillColor = true;
            if (_beDirty)
            {
                SizeF size = new SizeF();
                using (Graphics g = Graphics.FromHwnd(IntPtr.Zero))
                {
                    size = g.MeasureString(_text, _currentFont);
                }
                width = size.Width;
                height = size.Height;
                PointF org = new PointF(0.0f, 0.0f);
                _updateRender(size);
                _render.Clear(Util.HexColor(_backgroundColor, alpha));
                _render.DrawString(_text, _currentFont, Util.HexColorToBrush(_fontColor, alpha)/*Brushes.White*/, org);
            }
            _beDirty = false;
            GL.Enable(EnableCap.Texture2D);
            GL.BindTexture(TextureTarget.Texture2D, _render.Texture);
            base.render(deltaTime, true);
            GL.Disable(EnableCap.Texture2D);
        }

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {                    
                }
                if (_render != null)
                    _render.Dispose();
                _disposed = true;
            }
            base.Dispose(disposing);
        }
    }
}
