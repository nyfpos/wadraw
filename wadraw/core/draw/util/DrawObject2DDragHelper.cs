﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace wadraw
{
    internal class DrawObject2DDragHelper
    {
        private DrawObject2D _obj;
        private Point _offsetPoint;

        private bool _beStart;
        public bool beStart {
            get
            {
                return _beStart;
            }
            set
            {
                _beStart = value;
                if (_beStart)
                    start();
                else
                    stop();
            }
        }
        private bool _startDrag;

        public DrawObject2DDragHelper(DrawObject2D obj)
        {
            _obj = obj;
            _offsetPoint = new Point();
            _startDrag = false;
            beStart = false;
        }

        private void start()
        {
            _obj.addEventListener(EventType.MouseDown, _OnMouseDown);
            _obj.addEventListener(EventType.MouseUp, _OnMouseUp);
            _obj.addEventListener(EventType.CanvasMouseLeave, _OnMouseLeave);
           // _obj.addEventListener(EventType.MouseLeave, _OnMouseLeave);
            _obj.addEventListener(EventType.CanvasMouseMove, _OnMouseMove);            
        }


        private void _OnMouseDown(object sender, DrawObject dwobj, EventArgs e)
        {
            _startDrag = true;            
            _offsetPoint.x = Util.CursorPos().x - _obj.x;
            _offsetPoint.y = Util.CursorPos().y - _obj.y;
        }

        private void _OnMouseUp(object sender, DrawObject dwobj, EventArgs e)
        {
            _startDrag = false;
            _offsetPoint.x = _offsetPoint.y = 0;
        }

        private void _OnMouseLeave(object sender, DrawObject dwobj, EventArgs e)
        {
            _startDrag = false;
            _offsetPoint.x = _offsetPoint.y = 0;
        }

        private void _OnMouseMove(object sender, DrawObject dwobj, EventArgs e)
        {
            if (_startDrag)
            {
                float tmpX = Util.CursorPos().x - _offsetPoint.x;
                float tmpY = Util.CursorPos().y - _offsetPoint.y;

                //  no region restrictions
                /* 
                tmpX = tmpX < 0 ? 0 : tmpX;
                tmpY = tmpY < 0 ? 0 : tmpY;
                tmpX = tmpX > (_obj.canvasWidth - _obj.width) ? (_obj.canvasWidth - _obj.width) : tmpX;
                tmpY = tmpY > (_obj.canvasHeight - _obj.height) ? (_obj.canvasHeight - _obj.height) : tmpY;            
                 */ 
                _obj.x = tmpX;
                _obj.y = tmpY;                
            }
        }

        private void stop()
        {
            _obj.removeEventListener(EventType.MouseDown, _OnMouseDown);
            _obj.removeEventListener(EventType.MouseUp, _OnMouseUp);
            _obj.removeEventListener(EventType.CanvasMouseLeave, _OnMouseLeave);
            //_obj.removeEventListener(EventType.MouseLeave, _OnMouseLeave);
            _obj.removeEventListener(EventType.CanvasMouseMove, _OnMouseMove);            
        }

    }
}
