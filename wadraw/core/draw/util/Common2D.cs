﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wadraw
{
    public class Common2D
    {
        private static float[] _quad = null;
        public static float[] Quad
        {
            get
            {
                if (_quad == null)
                {
                    _quad = new float[]
                    {
                        -0.5f, 0.5f, 0.0f,
                        0.5f, 0.5f, 0.0f,
                        0.5f, -0.5f, 0.0f,
                        -0.5f, -0.5f, 0.0f
                    };

                }
                return _quad;
            }
        }

        public static float SystemLayerStep = 0.01f;
        public static float CommonLayerSetp = 0.00001f;

        public static float DefaultModelSize = 30.0f;

        static Common2D()
        {

        }


    }
}
