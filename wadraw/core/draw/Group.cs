﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wadraw
{
    public class Group : Quad
    {
        private float _tmpAlpaha;
        private List<DrawObject2D> _listObjs;

        public Group()
        {
            _tmpAlpaha = 0.0f;
            _listObjs = new List<DrawObject2D>();
            this.addEventListener(EventType.AddedCanvas, _OnAddedCanvas);
            this.addEventListener(EventType.RemovedCanvas, _OnRemovedCanvas);
        }

        private void _OnAddedCanvas(object sender, DrawObject dobj, EventArgs arg)
        {
            for (int i = 0; i < _listObjs.Count; i++)
            {
                //_listObjs[i].bindMatrix(ref viewMatrix, ref _projectMatrix);
            }
        }

        private void _OnRemovedCanvas(object sender, DrawObject dobj, EventArgs arg)
        {

        }

        public bool visableBorder
        {
            get;
            set;
        }

        public void addObject(DrawObject2D obj)
        {        
           _listObjs.Add(obj);
        }

        public void removeObject(DrawObject2D obj)
        {
            _listObjs.Remove(obj);
        }

        internal override void render(float deltaTime, bool hasTexture = false)
        {
            _tmpAlpaha = base.alpha;
            if (!visableBorder)
            {
                base.alpha = 0.0f;
            } else {
                base.alpha = _tmpAlpaha;
            }
            base.render(deltaTime, hasTexture);

            // >> >> >>

            // << << <<
        }
    }
}
