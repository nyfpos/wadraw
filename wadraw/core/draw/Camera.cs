﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wadraw
{
    public class Camera
    {
        public Vector3 eye;
        public Vector3 lookat;
        public Vector3 up;
    }
}
