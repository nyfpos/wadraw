﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;

namespace wadraw
{
    public class Sphere : DrawObject
    {
        public int numVertices;
        public int numTriangles;
        public int numIndices;
        public float radius;


        public Sphere(float r) :base()
        {
            _pivotLeftTop = new Vector3(0, 0, 0);            

            radius = r;
            _CreateSphere(null, 20, 20);
        }

        public Sphere(float[] color, float r) :base()
        {
            _pivotLeftTop = new Vector3(0, 0, 0);            

            radius = r;
            _CreateSphere(color, 20, 20);
        }

        public Sphere(float[] color, int stacks, int slices, float r)
            : base()
        {
            radius = r;
            _CreateSphere(color, stacks, slices);            
        }

        private bool _CreateSphere(float[] color, int stacks, int slices)
        {
            int num_vertices = (stacks + 1) * (slices + 1);
            int num_triangles = stacks * slices * 2;

            mesh.position = new float[3 * num_vertices];
            mesh.color = new byte[4 * num_vertices];
            indices = new uint[num_triangles * 3];

            numVertices = num_vertices;
            numTriangles = num_triangles;
            numIndices = num_triangles * 3;

            float[] default_color = new float[]
            {
                1.0f, 1.0f, 1.0f, 1.0f
            };

            if (color == null)
            {
                color = default_color;
            }

            const float theta_start_degree = 0.0f;
            const float theta_end_degree = 360.0f;
            const float phi_start_degree = -90.0f;
            const float phi_end_degree = 90.0f;

            float ts = Gut.Deg2Rad * theta_start_degree;
	        float te = Gut.Deg2Rad * theta_end_degree;
	        float ps = Gut.Deg2Rad * phi_start_degree;
	        float pe = Gut.Deg2Rad * phi_end_degree;

            float theta_total = te - ts;
            float phi_total = pe - ps;
            float theta_inc = theta_total / stacks;
            float phi_inc = phi_total / slices;

            int i, j;
            int index = 0;
            float theta = ts;

            float sin_theta, cos_theta;
            float sin_phi, cos_phi;

	        for ( i=0; i<=stacks; i++ )
	        {
		        float phi = ps;
		        
                sin_theta = (float)Math.Sin(theta);
                cos_theta = (float)Math.Cos(theta);

		        for ( j=0; j<=slices; j++, index++ )
		        {			       
                    sin_phi = (float)Math.Sin(phi);
                    cos_phi = (float)Math.Cos(phi);
			        // vertex
			        mesh.position[3*index+0] = radius * cos_phi * cos_theta;
			        mesh.position[3*index+1] = radius * sin_phi;
			        mesh.position[3*index+2] = radius * cos_phi * sin_theta;
			        // Color
			        float shading = (float) j / (float) slices;
			        // float shading = 1.0f;
			        mesh.color[3*index+0] = (byte)(255 * color[0] * shading);
			        mesh.color[3*index+index] = (byte)(255 * color[1] * shading);
			        mesh.color[3*index+index] = (byte)(255 * color[2] * shading);
			        mesh.color[3*index+index] = (byte)(255 * color[3] * shading);
			        // inc phi
			        phi += phi_inc;
		        }
		        // inc theta
		        theta += theta_inc;
	        }

	        int base_ = 0;
	        index = 0;
	
	        // triangle list
	        for ( i=0; i<stacks; i++ )
	        {
		        for ( j=0; j<slices; j++ )
		        {
                    indices[index++] = (uint)(base_);
                    indices[index++] = (uint)(base_ + 1);
                    indices[index++] = (uint)(base_ + slices + 1);

                    indices[index++] = (uint)(base_ + 1);
                    indices[index++] = (uint)(base_ + slices + 2);
                    indices[index++] = (uint)(base_ + slices + 1);

                    base_++;
		        }
                base_++;
	        }
            return true;
        }

        internal override void render(float deltaTime, bool hasTexture = false)
        {            
            defaultRender(PrimitiveType.Triangles, hasTexture);
        }
    }
}
