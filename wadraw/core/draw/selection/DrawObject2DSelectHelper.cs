﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using System.Windows.Forms;

namespace wadraw
{
    public enum SelectHelperMode
    {
        Normal,
        FrameOnly
    }

    public class DrawObject2DSelectHelper
    {
        private SelectionHover _hover;
        private SelectionFrame _frame;
        private Canvas _canvas;

        private Point _beginPt;        

        private bool _beSelecting;
        private bool _beMouseDown;

        private List<DrawObject2D> _selectedObjs;
        private bool _beSelected;
        private bool _beStart;
        private Dictionary<string, Vector2> _selectedOffset; // object_id, (offset.x,offset.y)

        private bool _enable;

        readonly private float MinScaleVal;

        private SelectHelperMode _mode;

        public DrawObject2DSelectHelper( Canvas canvas)
        {
            _selectedOffset = new Dictionary<string, Vector2>();
            _selectedObjs = new List<DrawObject2D>();            
            _hover = new SelectionHover(canvas);
            _frame = new SelectionFrame(canvas);
            _frame.start();
            _hover.start();
            _frame.enable = false;
            _canvas = canvas;
            _beStart = false;

            _beginPt = new Point();
            _beMouseDown = false;
            _beSelected = false;
            _enable = false;
            _beSelecting = false;
            MinScaleVal = 0.1f;
            
            _mode = SelectHelperMode.Normal;            
        }

        public DrawObject2D[] getSelectedObjects { get { return _selectedObjs.ToArray(); } }

        public SelectHelperMode mode
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;
                if (_mode == SelectHelperMode.Normal)
                {
                    _frame.visibleThumb = true;
                } else if (_mode == SelectHelperMode.FrameOnly)
                {
                    _frame.visibleThumb = false;
                }

                _hover.enable = _hover.enable;
                _frame.enable = _frame.enable;
            }
        }

        public Vector2 offsetXY
        {
            get
            {
                return _frame.offsetXY;
            }
            set
            {
                _frame.offsetXY = value;
            }
        }

        public uint color
        {
            get
            {
                return _frame.color;
            }
            set
            {
                _frame.color = value;
                _hover.color = value;
            }
        }

        public LineType frameLineType
        {
            get
            {
                return _frame.frameLineType;
            }
            set
            {                
                _frame.frameLineType = value;
            }
        }

        public bool beStart
        {
            get
            {
                return _beStart;
            }
            set
            {
                _beStart = value;
                if (_beStart)
                {
                    start();
                    enable = true;
                }
                else
                {
                    stop();
                    enable = false;
                }
            }
        }

        
        public bool enable
        {
            get
            {
                return _enable;
            }
            set
            {
                _enable = value;
                //_frame.enable = _enable;
                //_hover.enable = _enable;
                if (!_enable)
                {
                    _hover.enable = enable;
                    _frame.enable = enable;
                }
                else
                {
                    _beMouseDown = false;
                    _beSelected = false;
                    _beSelecting = false;
                }
            }
        }

        public float border
        {
            get
            {
                return _frame.border;
            }
            set
            {
                _frame.border = value;
                _hover.border = value;
            }
        }

        public void clear()
        {
            _hover.enable = _enable;
            _frame.enable = false;
            _beMouseDown = false;
            _beSelected = false;
            _beSelecting = false;
            _hover.mouseUp();
        }

        private void start()
        {
            _canvas.OnCanvasMouseDown += _OnCanvsMouseDown;
            _canvas.OnCanvasMouseUp += _OnCanvasMouseUp;

            _frame.addEventListener(EventType.SelectBegin, _OnSelectBegin);
            _frame.addEventListener(EventType.SelectEnd, _OnSelectEnd);
            _frame.addEventListener(EventType.SelectMoveBegin, _OnSelectBegin);
            _frame.addEventListener(EventType.SelectMoveEnd, _OnSelectEnd);
            _frame.addEventListener(EventType.SelectMoving, _OnSelectMoving);
            _frame.addEventListener(EventType.SelectScaling, _OnSelectScaling);
        }

        private void stop()
        {
            _canvas.OnCanvasMouseDown -= _OnCanvsMouseDown;
            _canvas.OnCanvasMouseUp -= _OnCanvasMouseUp;

            _frame.removeEventListener(EventType.SelectBegin, _OnSelectBegin);
            _frame.removeEventListener(EventType.SelectEnd, _OnSelectEnd);
            _frame.removeEventListener(EventType.SelectMoveBegin, _OnSelectBegin);
            _frame.removeEventListener(EventType.SelectMoveEnd, _OnSelectEnd);
            _frame.removeEventListener(EventType.SelectMoving, _OnSelectMoving);
            _frame.removeEventListener(EventType.SelectScaling, _OnSelectScaling);
        }

        private void _OnSelectScaling(object data, DrawObject dwobj, EventArgs evt)
        {
            if (!_enable) return;

            if (_beSelected)
            {
                List<float> lstF = new List<float>();
                Vector4 ratio = (Vector4)data;
                float w_rate = ratio[0];
                float h_rate = ratio[1];
                w_rate = Math.Max(w_rate, MinScaleVal);
                h_rate = Math.Max(h_rate, MinScaleVal);
                ThumbType thumbType = (ThumbType)((int)ratio[2]);                
               // GlLogger.Instance.LogInfo(string.Format("ratio:{0}:{1}", w_rate, h_rate));
                for (int i = 0; i < _selectedObjs.Count; i++)
                {
                    DrawObject2D obj = _selectedObjs[i];
                    obj.scaleX *= w_rate;
                    obj.scaleY *= h_rate;

                    Vector2 pos = Vector2.Zero;
                    if (_selectedOffset.TryGetValue(obj.ID, out pos))
                    {
                        pos[0] *= w_rate;
                        pos[1] *= h_rate;                        
                        obj.x = _frame.x + pos[0];
                        obj.y = _frame.y + pos[1];
                        _selectedOffset[obj.ID] = pos;
                    }

                    obj._resetRange();

                    lstF.Add(obj.leftTop.x);
                    lstF.Add(obj.leftTop.y);
                    lstF.Add(0.0f);
                    lstF.Add(obj.rightBottom.x);
                    lstF.Add(obj.rightBottom.y);
                    lstF.Add(0.0f);
                }
                float threshold = 1.0f;
                Point min = new Point(), max = new Point();
                Util.GetMinMaxPoint(lstF.ToArray(), out min, out max);
                if (Math.Abs(min.x - _frame.leftTopRightBottom[0]) > threshold ||
                     Math.Abs(min.y - _frame.leftTopRightBottom[1]) > threshold ||
                     Math.Abs(max.x - _frame.leftTopRightBottom[2]) > threshold ||
                     Math.Abs(max.y - _frame.leftTopRightBottom[3]) > threshold)
                {
                    _adjustWH(ref min, ref max);
                    
                    float org_x = _frame.x;
                    float org_y = _frame.y;

                    _frame.reset(min, max);
                    _resetSelectObjsOffset();  

                    // cannot figure out ... wtf
                    if (thumbType == ThumbType.BottomLeft ||
                        thumbType == ThumbType.BottomRight)
                    {
                        float diffx = _frame.x - org_x;
                        float diffy = _frame.y - org_y;
                        _frame.x = org_x;
                        _frame.y = org_y;
                        for (int i = 0; i < _selectedObjs.Count; i++)
                        {
                            DrawObject2D obj = _selectedObjs[i];
                            Vector2 pos = Vector2.Zero;
                            if (_selectedOffset.TryGetValue(obj.ID, out pos))
                            {
                                obj.x = _frame.x + pos[0];
                                obj.y = _frame.y + pos[1];
                            }
                        }
                    }
                                  
                }
                
            }
        }

        private void _OnSelectMoving(object sender, DrawObject dwobj, EventArgs evt)
        {
            if (!_enable) return;

            if (_beSelected)
            {
                for (int i = 0; i < _selectedObjs.Count; i++)
                {
                    DrawObject2D obj = _selectedObjs[i];
                    Vector2 pos = Vector2.Zero;
                    if ( _selectedOffset.TryGetValue(obj.ID, out pos) )
                    {
                        obj.x = _frame.x + pos[0];
                        obj.y = _frame.y + pos[1];
                    }
                }
            }
            else
            {
                GlLogger.Instance.LogInfo("_OnSelectMoving miss!");
            }
        }
        
        internal bool beHelperObjects(DrawObject2D obj)
        {
            return _frame.beFrameObject(obj) || _frame.beThumbObject(obj) || _hover.beHoverObject(obj);
        }

        private void _OnSelectBegin(object sender, DrawObject dwobj, EventArgs evt)
        {
            if (!_enable) return;

            bool beChangeSelectedObj = false;
            if (_beSelected)
            {
                
                float meX = Util.CursorPos().x;
                float meY = Util.CursorPos().y;
                DrawObject[] objs = _canvas.drawObject2DEventHelper.GetObjectsAtPoint(meX, meY);
                if (objs != null)
                { //beChangeSelectedObj = _selectedObjs.IndexOf(obj) < 0 ? true : false;
                    float maxZ = float.MinValue;
                    int maxZIndex = -1;
                    for (int i = 0; i < objs.Length; i++)
                    {
                        DrawObject2D obj = objs[i] as DrawObject2D;                        
                        if (obj != null)
                        {
                            if (_frame.beFrameObject(obj))
                                continue;
                            if (_frame.beThumbObject(obj))
                            {
                                beChangeSelectedObj = false;
                                break;
                            }
                            if (obj.z > maxZ)
                            {
                                maxZ = obj.z;
                                maxZIndex = i;
                                beChangeSelectedObj = true;
                            }
                        }
                    }
                    if (beChangeSelectedObj)
                    {
                        beChangeSelectedObj = false;
                        DrawObject2D obj = objs[maxZIndex] as DrawObject2D;
                        if (obj != null)
                            beChangeSelectedObj = _selectedObjs.IndexOf(obj) < 0 ? true : false;
                    }
                }                
            }

            if (!beChangeSelectedObj)
            {
                _beSelecting = true;
                _hover.enable = false;
            } else
            {
                _hover.enable = true;
                _frame.enable = false;
                _beSelecting = false;
                _beSelected = false;
            }
        }
        private void _OnSelectEnd(object sender, DrawObject dwobj, EventArgs evt)
        {
            if (!_enable) return;
            _beSelecting = false;            
        }

        private void _OnCanvsMouseDown(object sender, MouseEventArgs evt)
        {
            if (!_enable) return;

            if (!_beSelecting)
            {
                _hover.enable = true;
                _frame.enable = false;
                _beginPt.x = Util.CursorPos().x;
                _beginPt.y = Util.CursorPos().y;

                _beMouseDown = true;
            }

           // GlLogger.Instance.LogInfo(string.Format("x,y:{0},{1}", _beginPt.x, _beginPt.y));
        }

        private void _resetSelectObjsOffset()
        {
            _selectedOffset.Clear();
            for (int i = 0; i < _selectedObjs.Count; i++)
            {
                DrawObject2D obj = _selectedObjs[i];
                _selectedOffset[obj.ID] = new Vector2(obj.x - _frame.x, obj.y - _frame.y);                
            }
        }

        private void _OnCanvasMouseUp(object sender, MouseEventArgs evt)
        {
            if (!_enable) return;

            if (_beMouseDown)
            {
                _beMouseDown = false;

                Point minXY, maxXY;

                float meX = Util.CursorPos().x;
                float meY = Util.CursorPos().y;

                float[] pt = new float[]
                {
                    _beginPt.x, _beginPt.y, 0.0f,
                    meX, meY, 0.0f
                };
                Util.GetMinMaxPoint(pt, out minXY, out maxXY);

                float w = maxXY.x - minXY.x;
                float h = maxXY.y - minXY.y;
              /*  
                GlLogger.Instance.LogInfo(string.Format("meX,meY:{0},{1}", meX, meY));
                GlLogger.Instance.LogInfo(string.Format("w,h:{0},{1}", w, h));
               */

                DrawObject[] selectObjs = null;
                if (w <=0.0f || h <=0.0f)
                {
                    selectObjs = new DrawObject[]{_canvas.drawObject2DEventHelper.GetObjectAtPoint(meX, meY)};
                    if (selectObjs[0] == null)
                        selectObjs = null;
                }
                else
                {
                    selectObjs = _canvas.drawObject2DEventHelper.GetObjectsInRange(new Vector4(minXY.x, minXY.y, maxXY.x, maxXY.y));
                }

                _selectedObjs.Clear();
                if (selectObjs != null)
                {
                    List<float> lstF = new List<float>();
                   // GlLogger.Instance.LogInfo(string.Format("len:{0}", selectObjs.Length));

                    if (selectObjs.Length == 0)
                    {
                        _beSelected = false;
                        return;
                    }
                    
                    for (int i = 0; i < selectObjs.Length; i++)
                    {
                        if (selectObjs[i] == null)
                            continue;
                    //  GlLogger.Instance.LogInfo("selected:", selectObjs[i].name);
                        DrawObject2D obj2D = selectObjs[i] as DrawObject2D;
                        if (obj2D != null)
                        {
                            lstF.Add(obj2D.leftTop.x);
                            lstF.Add(obj2D.leftTop.y);
                            lstF.Add(0.0f);
                            lstF.Add(obj2D.rightBottom.x);
                            lstF.Add(obj2D.rightBottom.y);
                            lstF.Add(0.0f);
                            _selectedObjs.Add(obj2D);
                      //    GlLogger.Instance.LogInfo(string.Format("lt x,y,z:{0},{1},{2}", obj2D.leftTop.x, obj2D.leftTop.y, 0.0f));
                      //    GlLogger.Instance.LogInfo(string.Format("rb x,y,z:{0},{1},{2}", obj2D.rightBottom.x, obj2D.rightBottom.y, 0.0f));
                        }
                    }

                    Point min = new Point(), max = new Point();
                    Util.GetMinMaxPoint(lstF.ToArray(), out min, out max);

                    _adjustWH(ref min, ref max);
                    if (max.x - min.x >= 1.0f &&
                        max.y - min.y >= 1.0f)
                    {
                    //  GlLogger.Instance.LogInfo(string.Format("selected min:{0},{1}  max:{2},{3}", min.x, min.y, max.x, max.y));
                        _frame.reset(min, max);
                        _frame.enable = true;
                        _beSelected = true;
                        _resetSelectObjsOffset();
                    }
                    else {                        
                        _beSelected = false;
                    }
                }
            }
        }

        // adjust points for line object, since it may be zero of width or height
        private void _adjustWH(ref Point min, ref Point max)
        {
            float ws = max.x - min.x;
            float hs = max.y - min.y;
            if (hs <= 1.0f ) // for line
            {
                max.y += 3.0f;
                min.y -= 3.0f;

            }
            if (ws <= 1.0f)
            {
                max.x += 3.0f;
                min.x -= 3.0f;
            }
        }

    }
}
