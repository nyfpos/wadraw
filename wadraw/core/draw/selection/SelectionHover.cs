﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace wadraw
{
    public class SelectionHover : GlEventListener
    {
        private Canvas _canvas;

        private Quad _hover;

        private bool _beDown;
        private Point _p1;
        private Point _p2;

        private bool _beStart;
        private bool _enable;

        public SelectionHover(Canvas canvas)
        {
            _canvas = canvas;
            _hover = new Quad();
            _canvas.addObject(_hover);

            _hover.x = (float)_canvas.width * 0.5f;
            _hover.y = (float)_canvas.height * 0.5f;
            _hover.z = Common2D.SystemLayerStep + 0.07f;
            _hover.color = 0x3EF5A3;//0x20D080;
            _hover.fillColor = false;            
            _hover.visible = false;

            _beStart = false;
            _beDown = false;
            _p1 = new Point();
            _p2 = new Point();

            _enable = false;
        }

        public bool beHoverObject(DrawObject2D obj)
        {
            return obj == _hover;            
        }

        public void start()
        {            
            _beStart = true;                     
            _canvas.OnCanvasMouseDown += _OnCanvasMouseDown;
            _canvas.OnCanvasMouseUp += _OnCanvasMouseUp;
            _canvas.OnCanvasMouseMove += _OnCanvasMouseMove;            
        }

        public void stop()
        {
            _beStart = false;            
            _canvas.OnCanvasMouseDown -= _OnCanvasMouseDown;
            _canvas.OnCanvasMouseUp -= _OnCanvasMouseUp;
            _canvas.OnCanvasMouseMove -= _OnCanvasMouseMove;            
        }

        public void mouseUp()
        {
            _OnCanvasMouseUp(this, new EventArgs());
        }

        public uint color
        {
            get
            {
                return _hover.color;
            }
            set
            {
                _hover.color = value;
            }
        }

        public float border
        {
            get
            {
                return _hover.border;
            }
            set
            {
                _hover.border = value;
            }
        }

        public bool enable
        {
            get
            {
                return _enable;
            }
            set
            {
                _enable = value;                
                if (_enable)
                {
                    _hover.visible = true;
                    _hover.x = Util.CursorPos().x;
                    _hover.y = Util.CursorPos().y;
                    _hover.width = 1.0f;
                    _hover.height = 1.0f;
                    _OnCanvasMouseDown(null, null);
                }
                else
                    _hover.visible = false;
            }
        }

        private void _OnCanvasMouseDown(object sender, EventArgs e)
        {
            if (!_beStart || !enable) return;

           // MouseEventArgs me = e as MouseEventArgs;
            Point me = new Point(Util.CursorPos().x, Util.CursorPos().y);
            _p1 = new Point(me.x, me.y);
            _beDown = true;
            _hover.visible = true;
            _hover.x = me.x;
            _hover.y = me.y;
            _hover.width = 1.0f;
            _hover.height = 1.0f;
        }

        private void _OnCanvasMouseUp(object sender, EventArgs e)
        {
            if (!_beStart || !enable) return;
            _hover.visible = false;
            _beDown = false;
        }

        private void _OnCanvasMouseMove(object sender, EventArgs e)
        {
            if (!_beStart || !enable) return;            
            if (_beDown)
            {
                _p2 = new Point(Util.CursorPos().x, Util.CursorPos().y);
                _hover.reset(_p1, _p2);
            }
        }

    }
}
