﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using System.Windows.Forms;

namespace wadraw
{
    /*
    0 1 2
    3   4
    5 6 7
    */
    public enum ThumbType
    {
        None,
        TopLeft = 0,
        TopCenter,
        TopRight,

        MiddleLeft,
        MiddleRight,

        BottomLeft,
        BottomCenter,
        BottomRight
    }

    public class SelectionFrame : GlEventListener
    {
        private Quad[] _thumb;
        private Quad _frame;
        //private Group2D _selectObj;

        private Point _frameOffsetPos;
        private bool _beFrameDrag;

        private Canvas _canvas;

        private bool _beDrag;
        private Vector3 _beginPos;
        private Vector3 _prevPos;

        private float _thumbSize;

        private ThumbType _curThumbType;

        private bool _enable;

        private bool _visibleThumb;
        private uint _color;
        private Vector2 _offsetXY;

        public SelectionFrame(Canvas canvas)
        {
            _prevPos = Vector3.Zero;
            _beginPos = Vector3.Zero;
            _frameOffsetPos = new Point();
            _enable = false;

            _color = 0x3EF5A3;
            _offsetXY = Vector2.Zero;
            _beDrag = false;
            _canvas = canvas;
            _thumbSize = 8.0f;
            float frameSize = 30.0f;
            _curThumbType = ThumbType.None;

            _thumb = new Quad[]
            {
                new Quad(_thumbSize),
                new Quad(_thumbSize),
                new Quad(_thumbSize),
                new Quad(_thumbSize),
                new Quad(_thumbSize),
                new Quad(_thumbSize),
                new Quad(_thumbSize),
                new Quad(_thumbSize)
            };
            float halfFrameSize = frameSize * 0.5f;

            _frame = new Quad(frameSize);
            _frame.color = _color;

            _canvas.addObject(_frame);
            _frame.z = Common2D.SystemLayerStep + 0.05f;

            for (int i = 0; i < _thumb.Length; i++)
            {
                _canvas.addObject(_thumb[i]);
                _thumb[i].fillColor = false;
                _thumb[i].color = _color;
                _thumb[i].z = Common2D.SystemLayerStep + 0.06f;
                _thumb[i].name = "thumb_" + i.ToString();
            }

            _resetThumbPos();

            //   _frame.beDrag = true;
            _canvas.OnCanvasMouseMove += _OnCanvasMouseMove;
            _canvas.OnCanvasMouseUp += _OnCanvasMouseUp;
            
            _beFrameDrag = false;

            _visibleThumb = true;
        }

        public Vector2 offsetXY
        {
            get
            {
                return _offsetXY;
            }
            set
            {
                _offsetXY = value;
            }
        }

        public uint color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
                
                _frame.color = _color;
                for (int i = 0; i < _thumb.Length; i++)
                    _thumb[i].color = _color;
            }
        }

        public float border
        {
            get
            {
                return _frame.border;
            }
            set
            {
                _frame.border = value;
                for (int i = 0; i < _thumb.Length; i++)
                    _thumb[i].border = value;
            }
        }

        public void  start()
        {
            for (int i = 0; i < _thumb.Length; i++)
            {
                _thumb[i].addEventListener(EventType.MouseDown, _OnThumbMouseDown);
                _thumb[i].addEventListener(EventType.MouseUp, _OnThumbMouseUp);
            }
            _frame.addEventListener(EventType.MouseDown, _OnFrameMouseDown);
            _frame.addEventListener(EventType.MouseUp, _OnFrameMouseUp);            
        }

        public LineType frameLineType
        {
            get
            {
                return _frame.lineType;
            }
            set
            {
                _frame.lineType = value;
            }
        }

        public void stop()
        {
            for (int i = 0; i < _thumb.Length; i++)
            {
                _thumb[i].removeEventListener(EventType.MouseDown, _OnThumbMouseDown);
                _thumb[i].removeEventListener(EventType.MouseUp, _OnThumbMouseUp);
            }
            _frame.removeEventListener(EventType.MouseDown, _OnFrameMouseDown);
            _frame.removeEventListener(EventType.MouseUp, _OnFrameMouseUp);            
        }

        public bool enable
        {
            get
            {
                return _enable;
            }
            set
            {                
                _enable = value;                
                if (_enable)
                {
                    for (int i = 0; i < _thumb.Length; i++)
                        _thumb[i].visible = _visibleThumb;
                    _frame.visible = true;                    
                }
                else
                {                    
                    _enable = false;
                    for (int i = 0; i < _thumb.Length; i++)
                        _thumb[i].visible = false;
                    _frame.visible = false;
                }
                _beFrameDrag = false;
            }
        }

        public void reset(Point p1, Point p2)
        {
            if (_offsetXY != Vector2.Zero)
            {
                p1.x -= _offsetXY.X;
                p1.y -= _offsetXY.Y;
                p2.x += _offsetXY.X;
                p2.y += _offsetXY.Y;
            }
            _frame.reset(p1, p2);
            _resetThumbPos();
        }

        public bool visibleThumb
        {
            get
            {
                return _visibleThumb;
            }
            set
            {
                _visibleThumb = value;
                for (int i = 0; i < _thumb.Length; i++)
                    _thumb[i].visible = _visibleThumb;
            }
        }

        public float x
        {
            set
            {
                _frame.x = value;
                _resetThumbPos();
            }
            get
            {
                return _frame.x;
            }
        }

        public float y
        {
            set
            {
                _frame.y = value;
                _resetThumbPos();
            }
            get
            {
                return _frame.y;
            }
        }

        public float width
        {
            get
            {
                return _frame.width;
            }
        }

        public float height
        {
            get
            {
                return _frame.height;
            }
        }

        public Vector4 boundary
        {
            get
            {
                return _frame.boundary;
            }
        }

        internal bool beFrameObject(DrawObject2D obj)
        {
            if (obj == _frame)
            {
                return true;
            }
            return false;
        }

        internal bool beThumbObject(DrawObject2D obj)
        {
            for (int i = 0; i < _thumb.Length; i++)
            {
                if (_thumb[i] == obj)
                    return true;
            }
            return false;
        }

        public Vector4 leftTopRightBottom
        {
            get
            {
                return new Vector4(_frame.leftTop.x, _frame.leftTop.y,
                                   _frame.rightBottom.x, _frame.rightBottom.y);
            }
        }

        private void _OnFrameMouseDown(object sender, DrawObject dwobj, EventArgs evt)
        {
            if (!enable) return;
            _beFrameDrag = true;            
            _frameOffsetPos.x = Util.CursorPos().x - _frame.x;
            _frameOffsetPos.y = Util.CursorPos().y - _frame.y;
            this.Emit(sender, null, EventType.SelectMoveBegin, evt);
        }

        private void _OnFrameMouseUp(object sender, DrawObject dwobj, EventArgs evt)
        {
            if (!enable) return;
            _beFrameDrag = false;
            _frameOffsetPos.x = _frameOffsetPos.y = 0;
            this.Emit(sender, null, EventType.SelectMoveEnd, evt);
        }

        private void _resetThumbPos()
        {
            float halfThumbSize = _thumbSize * 0.5f;

            float w = _frame.width;
            float h = _frame.height;
            float hw = w * 0.5f;
            float hh = h * 0.5f;

            /*
             0 1 2
             3   4
             5 6 7
             */
            for (int i = 0; i < _thumb.Length; i++)
            {
                _thumb[i].x = _frame.x;
                _thumb[i].y = _frame.y;
            }
            _thumb[0].x = _thumb[0].x - halfThumbSize;
            _thumb[0].y = _thumb[0].y - halfThumbSize;
            _thumb[1].x = hw + _thumb[1].x - halfThumbSize;
            _thumb[1].y = _thumb[1].y - halfThumbSize;
            _thumb[2].x = w + _thumb[2].x - halfThumbSize;
            _thumb[2].y = _thumb[2].y - halfThumbSize;

            _thumb[3].x = _thumb[3].x - halfThumbSize;
            _thumb[3].y = _thumb[3].y + hh - halfThumbSize;
            _thumb[4].x = _thumb[4].x + w - halfThumbSize;
            _thumb[4].y = _thumb[4].y + hh - halfThumbSize;

            _thumb[5].x = _thumb[5].x - halfThumbSize;
            _thumb[5].y = _thumb[5].y - halfThumbSize + h;
            _thumb[6].x = _thumb[6].x - halfThumbSize + hw;
            _thumb[6].y = _thumb[6].y - halfThumbSize + h;
            _thumb[7].x = _thumb[7].x - halfThumbSize + w;
            _thumb[7].y = _thumb[7].y - halfThumbSize + h;
        }

        private float[] _getCurPosByThumb(float meX, float meY)
        {
            float diffx = meX - _prevPos.X;
            float diffy = meY - _prevPos.Y;
            float dist = (float)Math.Sqrt(diffx * diffx + diffy * diffy);
            if (diffx < 0)
                dist *= -1;

            float[] pos = null;          
            switch (_curThumbType)
            {
                case ThumbType.TopLeft:
                    {
                        pos = new float[] {
                                 _frame.x+dist, _frame.y+dist, 0.0f,                
                                 _frame.x+_frame.width, _frame.y +_frame.height, 0.0f
                            };
                    } break;
                case ThumbType.TopCenter:
                    {
                        pos = new float[] {
                                 _frame.x, meY, 0.0f,
                                 _frame.x+_frame.width, _frame.y +_frame.height, 0.0f
                            };
                    } break;
                case ThumbType.TopRight:
                    {                     
                        pos = new float[] {
                                 _frame.x+_frame.width+dist, _frame.y-dist, 0.0f,                    
                                 _frame.x, _frame.y +_frame.height, 0.0f
                            };
                    } break;
                case ThumbType.MiddleRight:
                    {
                        pos = new float[] {
                                 _frame.x, _frame.y, 0.0f,
                                 meX, _frame.y +_frame.height, 0.0f
                            };
                    } break;
                case ThumbType.MiddleLeft:
                    {
                        pos = new float[] {
                                 meX, _frame.y, 0.0f,
                                 _frame.x+_frame.width, _frame.y +_frame.height, 0.0f
                            };
                    } break;
                case ThumbType.BottomLeft:
                    {                     
                        pos = new float[] {
                                _frame.x+_frame.width, _frame.y , 0.0f,                                                                
                                _frame.x+dist, _frame.y + _frame.height-dist, 0.0f
                        };                       
                    } break;
                case ThumbType.BottomCenter:
                    {
                        pos = new float[] {
                                 _frame.x, _frame.y, 0.0f,
                                 _frame.x+_frame.width, meY, 0.0f
                            };
                    } break;
                case ThumbType.BottomRight:
                    {                      
                        pos = new float[] {
                               _frame.x, _frame.y, 0.0f,
                               _frame.x+_frame.width+dist, _frame.y+_frame.height+dist, 0.0f
                            };                       
                    } break;
            }
            return pos;
        }

        private void _OnCanvasMouseMove(object sender, MouseEventArgs me)
        {
            if (!enable) return;
            if (_beDrag)
            {
                _beFrameDrag = false;
                Point minXY = new Point(), maxXY = new Point();
                float meX = Util.CursorPos().x;
                float meY = Util.CursorPos().y;
           //   GlLogger.Instance.LogInfo(string.Format("beDrag:{0} xy:{1},{2}", _beDrag, meX, meY));
                float[] pos = _getCurPosByThumb(meX, meY);
                _prevPos = new Vector3(meX, meY, 0.0f);
                Util.GetMinMaxPoint(pos, out minXY, out maxXY);

                float w = maxXY.x - minXY.x;
                float h = maxXY.y - minXY.y;
                if (w >= 1.0f && h >= 1.0f)
                {
                    float prew = _frame.width;
                    float preh = _frame.height;
//                  GlLogger.Instance.LogInfo(string.Format("prew:{0} preh:{1}", prew, preh));
                    _frame.reset(minXY, maxXY);
                    float w_rate = _frame.width / prew;
                    float h_rate = _frame.height / preh;
                    _resetThumbPos();
                    Vector4 ratio = new Vector4(w_rate, h_rate, (int)_curThumbType, 0.0f);
                    this.Emit(ratio, null, EventType.SelectScaling, me);
                }                
                //GlLogger.Instance.LogInfo(string.Format("minXY.xy:{0},{1} | maxXY.xy:{2},{3}", minXY.x, minXY.y, maxXY.x, maxXY.y));
            }
            else if (_beFrameDrag)
            {
                _beDrag = false;

                float tmpX = Util.CursorPos().x - _frameOffsetPos.x;
                float tmpY = Util.CursorPos().y - _frameOffsetPos.y;
                //  no region restrictions
                /*
                tmpX = tmpX < 0 ? 0 : tmpX;
                tmpY = tmpY < 0 ? 0 : tmpY;
                tmpX = tmpX > (_frame.canvasWidth - _frame.width) ? (_frame.canvasWidth - _frame.width) : tmpX;
                tmpY = tmpY > (_frame.canvasHeight - _frame.height) ? (_frame.canvasHeight - _frame.height) : tmpY;
                 */
                _frame.x = tmpX;
                _frame.y = tmpY;                
                _resetThumbPos();
                this.Emit(sender, null, EventType.SelectMoving, me);
            }
        }

        private void _OnCanvasMouseUp(object sender, MouseEventArgs me)
        {
            if (!enable) return;
            _OnThumbMouseUp(null, null, me);
            _OnFrameMouseUp(null, null, me);
            this.Emit(sender, null, EventType.SelectEnd, me);
        }

        /*
          0 1 2
          7   3
          6 5 4
        */
        private void _OnThumbMouseDown(object sender, DrawObject dwobj, EventArgs evt)
        {
            if (!enable) return;            
            _beDrag = true;
            _beginPos.X = Util.CursorPos().x;
            _beginPos.Y = Util.CursorPos().y;
            _prevPos = _beginPos;
            int thumbno = int.Parse(dwobj.name.Replace("thumb_", ""));
            _curThumbType = (ThumbType)thumbno;
          //GlLogger.Instance.LogInfo(string.Format("mouseDown {0},{1} | curThumbType:{2}", _beginPos.X, _beginPos.Y, _curThumbType.ToString()));
            this.Emit(sender, null, EventType.SelectBegin, evt);
        }

        //===============================================================================//

        private void _OnThumbMouseUp(object sender, DrawObject dwobj, EventArgs evt)
        {
            if (!enable) return;
            _beDrag = false;
            _beginPos = Vector3.Zero;
            _prevPos = Vector3.Zero;
            _curThumbType = ThumbType.None;
        }
    }
}
