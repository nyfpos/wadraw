﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
namespace wadraw
{
    public class Quad : DrawObject2D
    {
        //private float _rounded;

        public Quad(float size=30.0f) :base()
        {            
            MeshDim = 3;
            indices = new uint[]{0,1,2,3};

            mesh.position = new float[]
            {
                -0.5f, 0.5f, 0.0f,
                0.5f, 0.5f, 0.0f,
                0.5f, -0.5f, 0.0f,
                -0.5f, -0.5f, 0.0f
            };

            const int meshRows = 4;
            mesh.color = new byte[meshRows * ColorDim];
            color = 0xFFFFFF;
            modelSize = size;
            float halfSize = size * 0.5f;
           // _rounded = 0;
            _pivotLeftTop = new Vector3(-halfSize, halfSize, 0);
            _pivotCenter = new Vector3(0, 0, 0);
            _pivotCenterBottom = new Vector3(0, -halfSize, 0);
        }

        internal override void render(float deltaTime, bool hasTexture = false)
        {
            defaultRender(PrimitiveType.Polygon, hasTexture);
        }
    }
}
