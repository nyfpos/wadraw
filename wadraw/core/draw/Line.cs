﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace wadraw
{
    public class Line : DrawObject2D
    {
        public Line(float size=30.0f) : base()
        {            
            MeshDim = 3;
            indices = new uint[] { 0,1 };
            mesh.position = new float[]
            {
                -0.5f, 0.0f, 0.0f,
                 0.5f, 0.0f, 0.0f
            };

            const int meshRows = 2;
            mesh.color = new byte[meshRows * ColorDim];
            color = 0xFFFFFF;
            modelSize = size;
            float halfSize = size * 0.5f;
            _pivotLeftTop = new Vector3(-halfSize, 0, 0);
            _pivotCenter = new Vector3(0, 0, 0);
        }

        internal override void render(float deltaTime, bool hasTexture = false)
        {
            defaultRender(PrimitiveType.Lines);
        }

        public override bool reset(Point p1, Point p2)
        {
            Vector3 vp1 = Util.ScreenToViewportPointFast(canvasInfo.width, canvasInfo.height, p1);
            Vector3 vp2 = Util.ScreenToViewportPointFast(canvasInfo.width, canvasInfo.height, p2);
            Vector3 dirV = vp2 - vp1;
            float d = dirV.Length;
            dirV.Normalize();            
            float theta = (float)Math.Acos(Vector3.Dot(dirV, Vector3.UnitX));                    
            Vector3 dirZ = Vector3.Cross(dirV, Vector3.UnitX);
            if (dirZ.Z > 0.0f) // clockwise
            {
                theta *= -1;
            }
            theta *= Gut.Rad2Deg;

            this.transform.clear();                     
            this.x = p1.x;
            this.y = p1.y;            
            this.width = d;
            this.height = 0;            
            this.rotateAroundZ(theta);
            return true;
        }
         
    }
}
