﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace wadraw
{    
    abstract public class DrawObject : GlEventListener, IDisposable
    {
        private bool _disposed;

        protected Matrix4 viewMatrix;
        protected Matrix4 projectMatrix;

        protected bool _beUpdateWorldMatrx;
        private Matrix4 _tmpWorldMatrix;

        public string name { get; set; }

        public object data { get; set; }

        public Transform transform;
        public Matrix4 worldMatrix
        {
            get
            {
                if (_beUpdateWorldMatrx || transform.beDirty)
                {
                    Vector3 pivotV = curPivot;
                    _tmpWorldMatrix = (Matrix4.CreateTranslation((-1.0f) * pivotV) *
                                       Matrix4.CreateScale(transform.scale) *
                                       Matrix4.CreateFromQuaternion(transform.rotation) *                                       
                                       Matrix4.CreateTranslation(transform.position));
                    _beUpdateWorldMatrx = false;
                    transform.beDirty = false;
                }
                return _tmpWorldMatrix;
            }
        }

        private bool _beDirty;

        private float _modelSize;
        internal float modelSize
        {
            get
            {
                return _modelSize;
            }
            set
            {
                if (_modelSize != value)
                {
                    _modelSize = value;
                    for (int i = 0; i < mesh.position.Length; i++)
                    {
                        mesh.position[i] *= _modelSize;
                    }
                }
            }
        }


        public Mesh mesh;
        public uint[] indices;

        public int TexCoordsDim { get; private set; }
        public int MeshDim { get; internal set; }
        public int ColorDim { get; private set; }

        protected Vector3 _pivotLeftTop;
        protected Vector3 _pivotCenter;
        protected Vector3 _pivotCenterBottom;

        protected Vector3 curPivot
        {
            get
            {
                if (pivotType == PivotType.LeftTop)
                {
                    return _pivotLeftTop;
                }
                else if (pivotType == PivotType.Center)
                {
                    return _pivotCenter;
                }
                else if (pivotType == PivotType.CenterBottom)
                {
                    return _pivotCenterBottom;
                }
                else
                {
                    return Vector3.Zero;
                }
            }
        }

        private PivotType _pivotType;
        public PivotType pivotType
        {
            get
            {
                return _pivotType;
            }
            set
            {
                if (_pivotType != value)
                {
                    _pivotType = value;
                    _beUpdateWorldMatrx = true;
                }
            }
        }

        public string ID
        {
            internal set;
            get;
        }

        public float x
        {
            
            get
            {
                return transform.position.X;
            }
            set
            {
                transform.position = new Vector3(value, transform.position.Y, transform.position.Z);
            }
        }

        public float y
        {
            get
            {
                return transform.position.Y;
            }
            set
            {
                transform.position = new Vector3(transform.position.X, value, transform.position.Z);
            }
        }

        public float z
        {
            get
            {
                return transform.position.Z;
            }
            set
            {
                transform.position =new Vector3(transform.position.X, transform.position.Y, value);
            }
        }

        protected uint _getColorWithAlpha(uint color, float alpha)
        {
            uint tmp = /*color &*/ 0xFF000000;
            byte alphaByte = (byte)(tmp >> 24);
            tmp = (byte)(alphaByte * alpha);
            tmp = tmp << 24;
            return (color & 0x00FFFFFF) | tmp;
        }

        private float _alpha;
        public float alpha
        {
            get
            {
                return _alpha;
            }
            set
            {
                _alpha = value;
                if (_alpha > 1)
                    _alpha = 1;
                if (_alpha < 0)
                    _alpha = 0;
                _beDirty = true;
                //colorARGB = _getColorWithAlpha(_color, _alpha);
            }
        }

        public bool visible
        {
            get;
            set;
        }

        /*
         // different border color
        glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
        draw_mesh( fill_color );
        glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
        glEnable(GL_POLYGON_OFFSET_LINE);
        glPolygonOffset(-1.f,-1.f);
        draw_mesh( line_color );
         */
        public bool fillColor
        {
            get;
            set;
        }

        public uint color
        {
            get
            {
                return _color & 0x00FFFFFF;
            }
            set
            {
                //  uint tmp = value & 0x00FFFFFF;
                //  colorARGB = (_color & 0xFF000000) | tmp;
                _beDirty = true;
                _color = value & 0x00FFFFFF;                
            }
        }

        private uint _color;
        protected uint colorARGB
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
                _color = _getColorWithAlpha(_color, _alpha);
                if (mesh.color != null )
                {
                   // 4 bytes
                    for (var i = 0; i < mesh.color .Length; i++)
                    {
                        // R,G,B,A in OpenGL
                      //  mesh.color [i] = (byte)_color;
                        uint tmp =0;
                        if (i % ColorDim == 0)
                        {
                            tmp = _color & 0x00FF0000;
                            mesh.color[i] = (byte)(tmp >> 16);
                            tmp = _color & 0x0000FF00;
                            mesh.color[i+1] = (byte)(tmp >> 8);                            
                            mesh.color[i+2] = (byte)(_color & 0x000000FF);
                            tmp = _color & 0xFF000000;
                            mesh.color[i+3] = (byte)(tmp >> 24);
                        }
                    }
                }
            }
        }

        internal bool beRender
        {
            get;
            set;
        }


        internal CanvasInfo canvasInfo;

        protected DrawBufferStatus _drawBuffStatus;
        private vbo _vbo;

        protected bool _beForceFillColor;

        public DrawObject() :base()
        {
            transform = new Transform();            
            visible = true;
            fillColor = false;
            colorARGB = 0xFFFFFFFF;
            alpha = 1.0f;
            pivotType = PivotType.LeftTop;

            _pivotLeftTop = new Vector3(-1, 1, 0);
            _pivotCenter = new Vector3(0, 0, 0);

            mesh.position = null;
            mesh.color = null;

            TexCoordsDim = 2;
            MeshDim = 3;
            ColorDim = 4;

            ID = Guid.NewGuid().ToString();

            _beForceFillColor = false;
            _beDirty = false;
            _beUpdateWorldMatrx = true;

            _drawBuffStatus = DrawBufferStatus.None;
            _vbo = null;

            _disposed = false;
            border = 1.0f;
            lineType = LineType.None;
            name = "";

            beRender = true;
        }

        internal void bindMatrix(ref Matrix4 view, ref Matrix4 proj)
        {
            viewMatrix = view;
            projectMatrix = proj;            
        }

        internal void updateBeforeRender()
        {
            if (!beRender)
                return;

            if (_disposed)
                return;

            if (_beDirty)
            {                
                colorARGB = _getColorWithAlpha(_color, _alpha);
                updateBuffer(UpdateBufferType.Color);
                _beDirty = false;
            }

            if (_drawBuffStatus == DrawBufferStatus.None)
            {
                _vbo = new vbo(mesh, indices);
                bool rs = _vbo.loadIntoGraphicsMemory();
                _drawBuffStatus = rs ? DrawBufferStatus.Loaded : DrawBufferStatus.FailedLoad;
            }
        }

        public float border
        {
            get;
            set;
        }

        public LineType lineType
        {
            get;
            set;
        }

        internal bool updateBuffer(UpdateBufferType utype)
        {
            if (_vbo == null)
            {
                return false;
            }
            if (_drawBuffStatus == DrawBufferStatus.Loaded &&
                _vbo.beLoadedIntoGPUMemory)
            {
                bool success = true;
                switch(utype)
                {
                    case UpdateBufferType.Color:
                        {
                            success = _vbo.genColorBuffer(mesh.color);
                        } break;
                    case UpdateBufferType.Indices:
                        {
                            success = _vbo.genIndices(indices);
                        } break;
                    case UpdateBufferType.Position:
                        {
                            success = _vbo.genPositionBuffer(mesh.position);
                        } break;
                    case UpdateBufferType.Texture:
                        {
                            success = _vbo.genTexBuffer(mesh.texCoords);
                        } break;
                }
                if (!success)
                {
                    _vbo.dispose();
                    _drawBuffStatus = DrawBufferStatus.FailedLoad;
                }
                return success;
            }
            return true;
        }

        internal abstract void render(float deltaTime, bool hasTexture = false);

        internal void defaultRender(PrimitiveType type, bool hasTexture=false)
        {
            if (!beRender)
                return;

            if (_disposed)
                return;

            if (lineType != LineType.None)
            {
                if (lineType == LineType.DashedLine)
                {
                    GL.Enable(EnableCap.LineStipple);
                    GL.LineStipple(2, 0x0F0F);
                } else if (lineType == LineType.DashedDotedLine)
                {
                    GL.Enable(EnableCap.LineStipple);
                    GL.LineStipple(2, 0x1c47);
                }
            }
            else
            {
                GL.Disable(EnableCap.LineStipple);
            }
            
            GL.LineWidth(border);
            
            if (fillColor || _beForceFillColor)
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            else
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);

            //if (beDepthTest)
            //  GL.Enable(EnableCap.DepthTest);

            
            GL.MatrixMode(MatrixMode.Modelview);
            Matrix4 worldViewMatrix = worldMatrix * viewMatrix;
            GL.LoadMatrix(ref worldViewMatrix);


            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.ColorArray);

            if (hasTexture)
                GL.EnableClientState(ArrayCap.TextureCoordArray);

            if (_drawBuffStatus == DrawBufferStatus.FailedLoad)
            {
                if (hasTexture && mesh.texCoords != null)
                    GL.TexCoordPointer(TexCoordsDim, TexCoordPointerType.Float, 0, mesh.texCoords);
                GL.VertexPointer(MeshDim, VertexPointerType.Float, 0, mesh.position);
                GL.ColorPointer(ColorDim, ColorPointerType.UnsignedByte, 0, mesh.color);
                GL.DrawElements(type, indices.Length, DrawElementsType.UnsignedInt, indices);
            }
            else if (_drawBuffStatus == DrawBufferStatus.Loaded)
            {
                if (hasTexture && mesh.texCoords != null)
                {
                    GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo.texCoordBufferID);                    
                    GL.TexCoordPointer(TexCoordsDim, TexCoordPointerType.Float, 0, IntPtr.Zero);
                }
                GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo.colorBufferID);
                GL.ColorPointer(ColorDim, ColorPointerType.UnsignedByte, 0, IntPtr.Zero);
                GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo.vertexBufferID);
                GL.VertexPointer(MeshDim, VertexPointerType.Float, 0, IntPtr.Zero);                
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, _vbo.elementBufferID);
                GL.DrawElements(type, indices.Length, DrawElementsType.UnsignedInt, IntPtr.Zero);
            }       
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {                 
                }
                if (_vbo != null && _vbo.beLoadedIntoGPUMemory)
                    _vbo.dispose();
                _disposed = true;
            }
        }

        ~DrawObject()
        {
            Dispose(false);
        }

    }

}
