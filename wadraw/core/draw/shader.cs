﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace wadraw
{
    public class Shader 
    {
        public enum VariableType
        {
            Uniform = 0,
            Attribute
        }

        public enum Type
        {
            Vertex = 0x1,
            Fragment = 0x2
        }

        public static bool IsSupported
        {
            get
            {
                return (new Version(GL.GetString(StringName.Version).Substring(0, 3)) >= new Version(2, 0) ? true : false);
            }
        }
        
        private int Program = 0;
        private Dictionary<string, int> Variables = new Dictionary<string, int>();
        private Dictionary<string, int> Buffers = new Dictionary<string, int>();

        public Shader(string source, Type type)
        {
            if (!IsSupported)
            {
                Console.WriteLine("Failed to create Shader." +
                    Environment.NewLine + "Your system doesn't support Shader.", "Error");
                return;
            }
            
            if (type == Type.Vertex)
                Compile(source, "");
            else
                Compile("", source);
        }

        public Shader(string vsource, string fsource)
        {
            if (!IsSupported)
            {
                Console.WriteLine("Failed to create Shader." +
                    Environment.NewLine + "Your system doesn't support Shader.", "Error");
                return;
            }
            
            Compile(vsource, fsource);
        }

        // I prefer to return the bool rather than throwing an exception lol
        private bool Compile(string vertexSource = "", string fragmentSource = "")
        {
            int status_code = -1;
            string info = "";

            if (vertexSource == "" && fragmentSource == "")
            {
                Console.WriteLine("Failed to compile Shader." +
                    Environment.NewLine + "Nothing to Compile.", "Error");
                return false;
            }

            if (Program > 0)
                GL.DeleteProgram(Program);

            Variables.Clear();

            Program = GL.CreateProgram();

            if (vertexSource != "")
            {
                int vertexShader = GL.CreateShader(ShaderType.VertexShader);
                GL.ShaderSource(vertexShader, vertexSource);
                GL.CompileShader(vertexShader);
                GL.GetShaderInfoLog(vertexShader, out info);
                GL.GetShader(vertexShader, ShaderParameter.CompileStatus, out status_code);

                if (status_code != 1)
                {
                    Console.WriteLine("Failed to Compile Vertex Shader Source." +
                        Environment.NewLine + info + Environment.NewLine + "Status Code: " + status_code.ToString());

                    GL.DeleteShader(vertexShader);
                    GL.DeleteProgram(Program);
                    Program = 0;

                    return false;
                }

                GL.AttachShader(Program, vertexShader);
                GL.DeleteShader(vertexShader);
            }

            if (fragmentSource != "")
            {
                int fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
                GL.ShaderSource(fragmentShader, fragmentSource);
                GL.CompileShader(fragmentShader);
                GL.GetShaderInfoLog(fragmentShader, out info);
                GL.GetShader(fragmentShader, ShaderParameter.CompileStatus, out status_code);

                if (status_code != 1)
                {
                    Console.WriteLine("Failed to Compile Fragment Shader Source." +
                        Environment.NewLine + info + Environment.NewLine + "Status Code: " + status_code.ToString());

                    GL.DeleteShader(fragmentShader);
                    GL.DeleteProgram(Program);
                    Program = 0;

                    return false;
                }

                GL.AttachShader(Program, fragmentShader);
                GL.DeleteShader(fragmentShader);
            }

            GL.LinkProgram(Program);
            GL.GetProgramInfoLog(Program, out info);
            GL.GetProgram(Program, GetProgramParameterName.LinkStatus, out status_code);

            if (status_code != 1)
            {
                Console.WriteLine("Failed to Link Shader Program." +
                    Environment.NewLine + info + Environment.NewLine + "Status Code: " + status_code.ToString());

                GL.DeleteProgram(Program);
                Program = 0;

                return false;
            }

            return true;
        }

        public bool GetAttributeIDs(string name, out int variableID, out int buffID)
        {
            buffID = -1;
            if (Variables.TryGetValue(name, out variableID))
            {
                if (Buffers.TryGetValue(name, out buffID))
                    return true;
            }
            return false;
        }

       // private int 
        public bool SetAttribute(string name, float[] attr)
        {
            if (Program > 0)
            {
                // shader variables...
                GL.UseProgram(Program);                
                int location = _GetVariableLocation(name, VariableType.Attribute);
                GL.UseProgram(0);
                if (location == -1)
                    return false;

                // vbo ...
                int bufferSize = 0;
                int buffID = -1;
                GL.GenBuffers(1, out buffID);
                GL.BindBuffer(BufferTarget.ArrayBuffer, buffID);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(attr.Length * sizeof(float)), attr, BufferUsageHint.DynamicDraw);
                GL.GetBufferParameter(BufferTarget.ArrayBuffer, BufferParameterName.BufferSize, out bufferSize);
                if (attr.Length * sizeof(float) != bufferSize)
                { 
                    GL.DeleteBuffers(1, ref buffID);
                    Variables.Remove(name);
                    return false;
                }
                Buffers.Add(name, buffID);
                GL.BindBuffer(BufferTarget.ArrayBuffer, 0);                
                return true;
            }
            else
                return false;
        }

        private int _GetVariableLocation(string name, VariableType vtype = VariableType.Uniform)
        {
            if (Variables.ContainsKey(name))
                return Variables[name];

            int location = -1;

            if (vtype == VariableType.Uniform)
                location = GL.GetUniformLocation(Program, name);
            else if (vtype == VariableType.Attribute)
                location = GL.GetAttribLocation(Program, name);

            if (location != -1)
                Variables.Add(name, location);
            else
                Console.WriteLine("Failed to retrieve Variable Location." +
                    Environment.NewLine + "Variable Name not found.", "Error");

            return location;
        }

        public void SetVariable(string name, float x)
        {
            if (Program > 0)
            {
                GL.UseProgram(Program);

                int location = _GetVariableLocation(name);
                if (location != -1)
                    GL.Uniform1(location, x);

                GL.UseProgram(0);
            }
        }

        public void SetVariable(string name, float x, float y)
        {
            if (Program > 0)
            {
                GL.UseProgram(Program);

                int location = _GetVariableLocation(name);
                if (location != -1)
                    GL.Uniform2(location, x, y);

                GL.UseProgram(0);
            }
        }

        public void SetVariable(string name, float x, float y, float z)
        {
            if (Program > 0)
            {
                GL.UseProgram(Program);

                int location = _GetVariableLocation(name);
                if (location != -1)
                    GL.Uniform3(location, x, y, z);

                GL.UseProgram(0);
            }
        }

        public void SetVariable(string name, float x, float y, float z, float w)
        {
            if (Program > 0)
            {
                GL.UseProgram(Program);

                int location = _GetVariableLocation(name);
                if (location != -1)
                    GL.Uniform4(location, x, y, z, w);

                GL.UseProgram(0);
            }
        }

        public void SetVariable(string name, Matrix4 matrix)
        {
            if (Program > 0)
            {
                GL.UseProgram(Program);

                int location = _GetVariableLocation(name);
                if (location != -1)
                {
                    // Well cannot use ref on lambda expression Lol
                    // So we need to call Check error manually
                    GL.UniformMatrix4(location, false, ref matrix);                    
                }

                GL.UseProgram(0);
            }
        }

        public void SetVariable(string name, Vector2 vector)
        {
            SetVariable(name, vector.X, vector.Y);
        }

        public void SetVariable(string name, Vector3 vector)
        {
            SetVariable(name, vector.X, vector.Y, vector.Z);
        }

        public void SetVariable(string name, Color color)
        {
            SetVariable(name, color.R / 255f, color.G / 255f, color.B / 255f, color.A / 255f);
        }                


        public static void Bind(Shader shader)
        {
            if (shader != null && shader.Program > 0)
            {
                GL.UseProgram(shader.Program);
            }
            else
            {
                GL.UseProgram(0);
            }
        }        

        public void Dispose()
        {
            foreach (var pair in Buffers)
            {
                int buffID = pair.Value;
                GL.DeleteBuffers(1, ref buffID);
            }
            if (Program != 0)
                GL.DeleteProgram(Program);
        }
    }
}