﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace wadraw
{
    internal class vbo
    {
        private float[] _position;
        private byte[] _color;
        private uint[] _indices;
        private float[] _texCoords;

        internal int colorBufferID;
        internal int vertexBufferID;
        internal int elementBufferID;
        internal int texCoordBufferID;

        public bool beLoadedIntoGPUMemory { get; private set; }

        public vbo(Mesh mesh, uint[] indices)
        {
            _position = mesh.position;
            _color = mesh.color;
            _texCoords = mesh.texCoords;
            _indices = indices;
            beLoadedIntoGPUMemory = false;
            colorBufferID =
            vertexBufferID =
            elementBufferID =
            texCoordBufferID = -1;
        }

        public bool genTexBuffer(float[] texCoords)
        {
            _texCoords = null;
            if (texCoordBufferID >= 0)
            {
                GL.DeleteBuffers(1, ref texCoordBufferID);
                texCoordBufferID = -1;
            }
            int bufferSize = 0;
            if (texCoords != null)
            {
                // Generate Array Buffer Id
                GL.GenBuffers(1, out texCoordBufferID);
                // Bind current context to Array Buffer ID
                GL.BindBuffer(BufferTarget.ArrayBuffer, texCoordBufferID);
                // Send data to buffer
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(texCoords.Length * sizeof(float)), texCoords, BufferUsageHint.StaticDraw);
                // Validate that the buffer is the correct size
                GL.GetBufferParameter(BufferTarget.ArrayBuffer, BufferParameterName.BufferSize, out bufferSize);
                if (texCoords.Length * sizeof(float) != bufferSize)
                { //   throw new ApplicationException("TexCoord array not uploaded correctly");
                    GL.DeleteBuffers(1, ref texCoordBufferID);
                    texCoordBufferID = -1;
                    return false;
                }
                // Clear the buffer Binding
                GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            }
            _texCoords = texCoords;
            return true;
        }

        public bool genColorBuffer(byte[] color)
        {
            _color = null;
            if (colorBufferID >= 0)
            {
                GL.DeleteBuffers(1, ref colorBufferID);
                colorBufferID = -1;
            }
            int bufferSize = 0;
            if (color != null)
            {
                // Generate Array Buffer Id
                GL.GenBuffers(1, out colorBufferID);
                // Bind current context to Array Buffer ID
                GL.BindBuffer(BufferTarget.ArrayBuffer, colorBufferID);
                // Send data to buffer
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(color.Length * sizeof(byte)), color, BufferUsageHint.DynamicDraw);
                // Validate that the buffer is the correct size
                GL.GetBufferParameter(BufferTarget.ArrayBuffer, BufferParameterName.BufferSize, out bufferSize);
                if (color.Length * sizeof(byte) != bufferSize)
                { //throw new ApplicationException("Vertex array not uploaded correctly");
                    GL.DeleteBuffers(1, ref colorBufferID);
                    colorBufferID = -1;
                    return false;
                }
                // Clear the buffer Binding
                GL.BindBuffer(BufferTarget.ArrayBuffer, 0);                
            }
            _color = color;
            return true;
        }

        public bool genPositionBuffer(float[] position)
        {
            _position = null;
            if (vertexBufferID >= 0)
            {
                GL.DeleteBuffers(1, ref vertexBufferID);
                vertexBufferID = -1;
            }
            int bufferSize = 0;
            if (position != null)
            {
                // Generate Array Buffer Id
                GL.GenBuffers(1, out vertexBufferID);

                // Bind current context to Array Buffer ID
                GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferID);

                // Send data to buffer
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(position.Length * sizeof(float)), position, BufferUsageHint.DynamicDraw);

                // Validate that the buffer is the correct size
                GL.GetBufferParameter(BufferTarget.ArrayBuffer, BufferParameterName.BufferSize, out bufferSize);
                if (position.Length * sizeof(float) != bufferSize)
                { // throw new ApplicationException("Vertex array not uploaded correctly");
                    GL.DeleteBuffers(1, ref vertexBufferID);
                    vertexBufferID = -1;
                    return false;
                }
                // Clear the buffer Binding
                GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            }
            _position = position;
            return true;
        }

        public bool genIndices(uint[] indices)
        {
            _indices = null;
            if (elementBufferID >= 0)
            {
                GL.DeleteBuffers(1, ref elementBufferID);
                elementBufferID = -1;
            }
            int bufferSize = 0;
            if (indices != null)
            {
                // Generate Array Buffer Id
                GL.GenBuffers(1, out elementBufferID);

                // Bind current context to Array Buffer ID
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, elementBufferID);

                // Send data to buffer
                GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(indices.Length * sizeof(uint)), indices, BufferUsageHint.StaticDraw);

                // Validate that the buffer is the correct size
                GL.GetBufferParameter(BufferTarget.ElementArrayBuffer, BufferParameterName.BufferSize, out bufferSize);
                if (indices.Length * sizeof(uint) != bufferSize)
                { //   throw new ApplicationException("Element array not uploaded correctly");
                    GL.DeleteBuffers(1, ref elementBufferID);
                    elementBufferID = -1;
                    return false;
                }
                // Clear the buffer Binding
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            }
            _indices = indices;
            return true;
        }        

        public bool loadIntoGraphicsMemory()
        {
            if(!genTexBuffer(_texCoords))
            {
                return false;
            }

            if (!genColorBuffer(_color))
            {
                return false;
            }

            if (!genPositionBuffer(_position))
            {
                return false;
            }

            if (!genIndices(_indices))
            {
                return false;
            }

            beLoadedIntoGPUMemory = true;
            return true;
        }

        public void dispose()
        {
            if (beLoadedIntoGPUMemory)
            {
                beLoadedIntoGPUMemory = false;
                if (colorBufferID>=0)
                    GL.DeleteBuffers(1, ref colorBufferID);
                if (vertexBufferID>=0)
                    GL.DeleteBuffers(1, ref vertexBufferID);
                if (elementBufferID>=0)
                    GL.DeleteBuffers(1, ref elementBufferID);
                if (texCoordBufferID>=0)
                    GL.DeleteBuffers(1, ref texCoordBufferID);
            }
        }
    }
}
