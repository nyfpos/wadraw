﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace wadraw
{
    public class PolyLine : DrawObject2D
    {
        private List<Point> _screenPoints;

        private bool _beDirty;        

        public PolyLine(float size = 30.0f) : base()
        {
            _screenPoints = new List<Point>();
            _beDirty = true;
            MeshDim = 3;
            indices = null;
            mesh.position = null;
            beRender = false;

            color = 0xFFFFFF;                
        }

        public void addPoint(Point p)
        {
            _screenPoints.Add(p);            
            _beDirty = true;
        }

        public void addPath(Point[] points)
        {
            for (int i = 0; i < points.Length; i++)
                _screenPoints.Add(points[i]);
        }

        public void clear()
        {
            _screenPoints.Clear();
            _beDirty = true;
            beRender = false;
        }

        internal override void render(float deltaTime, bool hasTexture = false)
        {
            if ((mesh.position == null || _beDirty) &&
                _screenPoints.Count >= 2)
            {
                int numpt = (_screenPoints.Count - 2) * 2 + 2;
                float[] tmppos = new float[numpt * MeshDim];
                mesh.position = new float[numpt * MeshDim];
                indices = new uint[numpt];
                uint m = 0;
                for (int i = 0, j=0; i < _screenPoints.Count; i++)
                {
                    int kl = i==0 || i == _screenPoints.Count-1 ?1:2;
                    for (int k = 0; k < kl; k++)
                    {
                        tmppos[j++] = _screenPoints[i].x;
                        tmppos[j++] = _screenPoints[i].y;
                        tmppos[j++] = 0.0f;
                        indices[m] = m++;
                    }
                }
                Point maxPoint, minPoint;
                Util.GetMinMaxPoint(tmppos, out minPoint, out maxPoint);
                const int dimension = 3;
                for (int i = 0; i < mesh.position.Length ; i++)
                {
                    if (i % dimension == 0)
                    {
                        mesh.position[i]   = Util.ScreenToViewportPointFast_X(canvasInfo.width, tmppos[i]);
                        mesh.position[i+1] = Util.ScreenToViewportPointFast_Y(canvasInfo.height, tmppos[i+1]);
                        mesh.position[i+2] = tmppos[i+2];
                    }
                }
                // normalize...
                float offsetx = mesh.position[0],
                      offsety = mesh.position[1];
                for (int i = 0; i < mesh.position.Length; i++)
                {
                    if (i % dimension == 0)
                    {
                        mesh.position[i] -= offsetx;
                        mesh.position[i+1] -= offsety;
                    }
                }
                mesh.color = new byte[numpt * ColorDim];
                colorARGB = _getColorWithAlpha(color, alpha);
                if (_drawBuffStatus != DrawBufferStatus.None)
                {                
                    updateBuffer(UpdateBufferType.Position);
                    updateBuffer(UpdateBufferType.Color);
                    updateBuffer(UpdateBufferType.Indices);
                }                
                _beDirty = false;
                beRender = true;
                x = minPoint.x;
                y = minPoint.y;
            }
            else if (beRender)
            {
                defaultRender(PrimitiveType.Lines);                
            }
        }
    }
}