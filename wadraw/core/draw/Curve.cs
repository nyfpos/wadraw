﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using System.IO;

namespace wadraw
{
    public class Curve : DrawObject2D
    {
        private Shader _shader;
        private bool _disposed;

        private float _thickness;

        public float thickness
        {
            get
            {
                return _thickness;
            }
            set
            {
                _thickness = value;
            }
        }

        public Curve(float size = 30.0f):base()
        {
            _disposed = false;
            MeshDim = 3;

            /*
            indices = new uint[] { 0, 1, 2 };
            mesh.position = new float[]
            {
			    -0.5f, -0.5f,  0.0f,
				 0.0f,  0.5f,  0.0f,
				 0.5f, -0.5f,  0.0f,
            };
             */ 
            indices = new uint[] { 0, 1, 2, 3, 4, 5 };
            mesh.position = new float[]
            {
                -1.0f, 0.0f, 0.0f,
                -0.5f, 0.5f, 0.0f,
                 0.0f, 0.0f, 0.0f,

                 0.0f, 0.0f, 0.0f,
                 0.5f,-0.5f, 0.0f,
                 1.0f, 0.0f, 0.0f
            };

            modelSize = size;
            float halfSize = size * 0.5f;
            const int meshRows = 6;
            mesh.color = new byte[meshRows * ColorDim];
            color = 0xFF0000;

            _pivotLeftTop = new Vector3(-halfSize, 0, 0);
            _pivotCenter = new Vector3(0, 0, 0);

            _thickness = 1.0f;

            float[] bezierCoords = new float[]
            {
				1.0f,  1.0f,
				0.5f,  0.0f,
				0.0f,  0.0f,

                0.0f,  0.0f,
				0.5f,  0.0f,				
                1.0f,  1.0f
			};

            string currentDir = Directory.GetCurrentDirectory();
            string vs, fs;
            vs = System.IO.File.ReadAllText(Path.Combine(currentDir, @"./shader/curve_vertex.glsl"));
            fs = System.IO.File.ReadAllText(Path.Combine(currentDir,@"./shader/curve_fragment.glsl"));
            _shader = new Shader(vs, fs);
            _shader.SetAttribute("aBezierCoord", bezierCoords);
            _shader.SetVariable("uThickness", _thickness);            
        }

        internal override void render(float deltaTime, bool hasTexture = false)
        {            
            _shader.SetVariable("fillColor", fillColor ? 1.0f : 0.0f);

            Shader.Bind(_shader);            
            int variableID;
            int buffID;
            if (_shader.GetAttributeIDs("aBezierCoord", out variableID, out buffID))
            {
                int bezierCoordsItemSize = 2;
//              int bezierCoordsNumItems = 3;
                GL.EnableVertexAttribArray(variableID);
                GL.BindBuffer(BufferTarget.ArrayBuffer, buffID);
                GL.VertexAttribPointer(variableID, bezierCoordsItemSize, VertexAttribPointerType.Float, false, 0, 0);
            }

            _beForceFillColor = true;
            defaultRender(PrimitiveType.Triangles);
            Shader.Bind(null);
        }

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                }
                if (_shader!= null)
                    _shader.Dispose();
                _disposed = true;
            }
            base.Dispose(disposing);
        }
    }
}
