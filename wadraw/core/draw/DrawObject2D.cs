﻿using Newtonsoft.Json.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;

namespace wadraw
{
    abstract public class DrawObject2D : DrawObject
    {
        private bool _beDirtyRotate;
        private bool _beDirty;
        private Vector4 _leftTopRightBottom;

        private Vector4 _meshLTRB;
        internal float[] _meshRect;

        internal bool beHover {get; set;}
        
        private bool _beInitedRange;

        private const float MinScaleValue = 0.1f;

        internal Vector2 _tmpXY;

        private Vector2 _orgWH;

        private SerializableObject _serailizeObj;

        private DrawObject2DDragHelper _dragHelper;

        /*
            the properties x, y, width, height, leftTop, rightBottom,..  of this class is relative with Screen Point
         */ 
        public DrawObject2D() : base()
        {
            _dragHelper = new DrawObject2DDragHelper(this);
            _orgWH = Vector2.Zero;
            _tmpXY = new Vector2();
            beHover = false;
            _beDirty = true;            
            _leftTopRightBottom = new Vector4();
            _meshLTRB = new Vector4();            
            _pivotType = PivotType.LeftTop;
            _beInitedRange = false;
            _beDirtyRotate = true;
            _serailizeObj = new SerializableObject(this);
        }

        public bool beDrag
        {
            get
            {
                return _dragHelper.beStart;
            }
            set
            {
                _dragHelper.beStart = value;
            }
        }

        private PivotType _pivotType;
        public new PivotType pivotType
        {
            get
            {
                return _pivotType;
            }
            set
            {
                if (_pivotType != value)
                {
                    _beDirtyRotate = true;
                    _beUpdateWorldMatrx = true;
                    _pivotType = value;
                    base.pivotType = value;
                    _resetRange();
                }                
            }
        }

        internal new void defaultRender(PrimitiveType type, bool hasTexture = false)
        {
            base.defaultRender(type, hasTexture);
            if (_beDirty) _resetRange();
        }


        public new float x
        {

            get
            {
                return Util.ViewportToScreenPointFast_X(canvasInfo.width, transform.position.X);
            }
            set
            {
                _tmpXY = new Vector2(value, _tmpXY.Y);
                transform.position = new Vector3(Util.ScreenToViewportPointFast_X(canvasInfo.width, value), transform.position.Y, transform.position.Z);
                _beDirty = true;          
            }
        }

        public new float y
        {
            get
            {
                return Util.ViewportToScreenPointFast_Y(canvasInfo.height, transform.position.Y);
            }
            set
            {
                _tmpXY = new Vector2(_tmpXY.X, value);
                transform.position = new Vector3(transform.position.X, Util.ScreenToViewportPointFast_Y(canvasInfo.height, value), transform.position.Z);
                _beDirty = true;
            }
        }

        public new float z
        {
            get
            {
                return base.z;
            }
            set
            {
                base.z = value;
            }
        }

        public void rotateAroundZ(float degreeZ)
        {           
            Vector3 degrees = Gut.FromQ2(transform.rotation);
            transform.rotation = Gut.ToQ(degrees.X, degrees.Y, degreeZ);
            _beDirty = true;
            _beDirtyRotate = true;
        }

        // degree
        public float getRotatedDegreeZ()
        {
            Vector3 degrees = Gut.FromQ2(transform.rotation);
            return degrees.Z;
        }

        public float scaleX
        {
            get
            {
                return transform.scale.X;
            }
            set
            {
                transform.scale = new Vector3(value < MinScaleValue ? MinScaleValue : value, transform.scale.Y, transform.scale.Z);
                _beDirty = true;                
            }
        }

        public float scaleY
        {
            get
            {
                return transform.scale.Y;
            }
            set
            {
                transform.scale = new Vector3(transform.scale.X, value < MinScaleValue ? MinScaleValue : value, transform.scale.Z);
                _beDirty = true;
             //   GlLogger.Instance.LogInfo(string.Format("scale.X:{0} scale.Y:{1}", transform.scale.X, transform.scale.Y));
            }
        }

        public float width
        {
            get
            {
                if (_beDirty) _resetRange();
                /*
                Vector2 wh = _getOrgWH();
                return wh[0] * scaleX;
                 * */
                return _leftTopRightBottom[2] - _leftTopRightBottom[0];
            }
            set
            {
                if (_beDirty) _resetRange();
                Vector2 wh = _getOrgWH();
                float w = wh[0];
                float tmpval = value;
                if (tmpval <= 0)
                    tmpval = w;
                scaleX = tmpval / w;
                _beDirty = true;
            }
        }
        
        public float height
        {
            get
            {
                if (_beDirty) _resetRange();
                /*
                Vector2 wh = _getOrgWH();
                return wh[1] * scaleY;
                 * */
                return _leftTopRightBottom[3] - _leftTopRightBottom[1];
            }
            set
            {
                if (_beDirty) _resetRange();
                Vector2 wh = _getOrgWH();
                float h = wh[1];
                float tmpval = value;
                if (tmpval <= 0)
                    tmpval = h;
                scaleY = tmpval / h;
                _beDirty = true;
            }
        }

        internal void _resetRange()
        {
            if (!_beInitedRange)
            {
                if (mesh.position.Length >= 4)
                {
                    Util.GetLeftTopRightBottom3D(mesh.position, out _meshLTRB);
                    _meshRect = new float[] 
                    {
                        _meshLTRB[0], _meshLTRB[1], 0.0f,
                        _meshLTRB[2], _meshLTRB[1], 0.0f,
                        _meshLTRB[0], _meshLTRB[3], 0.0f,
                        _meshLTRB[2], _meshLTRB[3], 0.0f,
                    };
                }
                else
                {
                    _meshRect = new float[mesh.position.Length];
                    for (int i = 0; i < mesh.position.Length; i++)
                    {
                        _meshRect[i] = mesh.position[i];
                    }
                }
            }

            /****
             *  note: in 3D -space 
             *  leftTop:     (minX, maxY)
             *  rightBottom: (maxX, minY)
             */           
            Vector3 pivotV = curPivot;
            Matrix4 transformMatrix = worldMatrix;
            float[] transformedPoint = Util.TransformEachPoint(_meshRect, ref transformMatrix, MeshDim);
            Vector4 tmp;
            Util.GetLeftTopRightBottom3D(transformedPoint, out tmp);
            _leftTopRightBottom[0] = Util.ViewportToScreenPointFast_X(canvasInfo.width, tmp[0]);
            _leftTopRightBottom[1] = Util.ViewportToScreenPointFast_Y(canvasInfo.height, tmp[1]);
            _leftTopRightBottom[2] = Util.ViewportToScreenPointFast_X(canvasInfo.width, tmp[2]);
            _leftTopRightBottom[3] = Util.ViewportToScreenPointFast_Y(canvasInfo.height, tmp[3]);

            _beDirty = false;
            _beInitedRange = true;
        }

        public Vector4 boundary
        {
            get
            {
                return _leftTopRightBottom;
            }
        }
                
        public Point leftTop
        {            
            get
            {
                if (_beDirty)
                    _resetRange();                
                return new Point(_leftTopRightBottom[0], _leftTopRightBottom[1]);
            }             
        }

        public Point rightBottom
        {
            get
            {
                if (_beDirty)
                    _resetRange();
                return new Point(_leftTopRightBottom[2], _leftTopRightBottom[3]);
            }
        }

        public virtual JObject toJObject() { return _serailizeObj.toJObject(); }
        public virtual void fromJObject(JObject jobj) { _serailizeObj.fromJObject(jobj); }
        public virtual DrawObject2D Clone()
        {
            DrawObject2D dest = null;
            if (this is DrawText)
                dest = (DrawObject2D)Activator.CreateInstance(this.GetType());
            else if (this is DrawImage)
            {
                DrawImage org = this as DrawImage;
                dest = (DrawObject2D)Activator.CreateInstance(this.GetType(), org.src);
            }
            else
                dest = (DrawObject2D)Activator.CreateInstance(this.GetType(), modelSize);
          //dest.bindMatrix(ref this.viewMatrix, ref this.projectMatrix);
            dest.canvasInfo.width = Util.getCanvasWidth();//this.canvasWidth;
            dest.canvasInfo.height = Util.getCanvasHeight();//this.canvasHeight;

            dest.color = this.color;
            dest.alpha = this.alpha;

            dest.fillColor = this.fillColor;
            
            dest.x = this.x;
            dest.y = this.y;
            dest.z = this.z;
            dest.scaleX = this.scaleX;
            dest.scaleY = this.scaleY;
            dest.rotateAroundZ(this.getRotatedDegreeZ());            

            return dest;
        }

        public bool reset (float p1x, float p1y, float p2x, float p2y)
        {
            return reset(new Point(p1x, p1y), new Point(p2x, p2y));
        }

        public virtual bool reset(Point p1, Point p2)
        {
            if (p1.x == p2.x && p1.y == p2.y)
                return false;
            Point minXY, maxXY;
            float[] pos = new float[]{
                p1.x, p1.y, 0.0f,
                p2.x, p2.y, 0.0f
            };
            Util.GetMinMaxPoint(pos, out minXY, out maxXY);
            float w = maxXY.x - minXY.x;
            float h = maxXY.y - minXY.y;
            this.x = minXY.x;
            this.y = minXY.y;
            this.width = w;
            this.height = h;
            return true;
        }
        
        private Vector2 _getOrgWH()
        {
            if (_beDirtyRotate)
            {
                Vector3 pivotV = curPivot;
                Matrix4 transformMatrix = (Matrix4.CreateTranslation((-1.0f) * pivotV) *
                                           Matrix4.CreateFromQuaternion(transform.rotation));
                float[] transformedPoint = Util.TransformEachPoint(_meshRect, ref transformMatrix, MeshDim);
                Vector4 tmp;
                Util.GetLeftTopRightBottom3D(transformedPoint, out tmp);                
                _orgWH[0] = Util.ViewportToScreenPointFast_X(canvasInfo.width, tmp[2]) - Util.ViewportToScreenPointFast_X(canvasInfo.width, tmp[0]);
                _orgWH[1] = Util.ViewportToScreenPointFast_Y(canvasInfo.height, tmp[3]) - Util.ViewportToScreenPointFast_Y(canvasInfo.height, tmp[1]);
                _beDirtyRotate = false;
            }
            return new Vector2(_orgWH[0]<= 0?1:_orgWH[0], _orgWH[1]<= 0?1:_orgWH[1]);
        }
        
    }   
}
