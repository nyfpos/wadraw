﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace wadraw
{
    public class DrawImage : Quad
    {
        private Bitmap _bmp;
        private int _texture;
        private bool _disposed;
        private string _src;             

        public DrawImage(string path) : base()
        {            
            _src = path;                        
            _disposed = false;


            mesh.texCoords = new float[]
            {
                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f,
                0.0f, 1.0f
            };

            _loadTexture(path, out _texture, out _bmp);

            if (_bmp != null)
            {
                width = _bmp.Width;
                height = _bmp.Height;
            }

        }

        public Bitmap bitmap
        {
            get
            {
                return _bmp;
            }
        }

        public void LoadTexture(Bitmap bitmap)
        {
            if (_bmp != null)
                _bmp.Dispose();
            if (GraphicsContext.CurrentContext != null && _texture != -1)
                GL.DeleteTexture(_texture);
            _bmp = new Bitmap(bitmap);
            __loadTexture(ref _bmp, out _texture);

            if (_bmp != null)
            {
                width = _bmp.Width;
                height = _bmp.Height;
            }
        }

        private void __loadTexture(ref Bitmap bitmap, out int tex)
        {
            tex = -1;
            try
            {
                GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);

                GL.GenTextures(1, out tex);
                GL.BindTexture(TextureTarget.Texture2D, tex);

                BitmapData data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                    ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0,
                    OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
                bitmap.UnlockBits(data);


                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
                //                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
                //                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
            }
            catch (Exception ex)
            {
                bitmap = null;
                tex = -1;
                GlLogger.Instance.Error(ex.Message);
            }
            finally
            {

            }
        }

        private void _loadTexture(string filename, out int tex, out Bitmap bitmap)
        {
            bitmap = null;
            tex = -1;
            try
            {
                if (String.IsNullOrEmpty(filename))
                    throw new ArgumentException(filename);
                bitmap = new Bitmap(filename);
                __loadTexture(ref bitmap, out tex);
            }
            catch (Exception ex)
            {
                bitmap = null;
                tex = -1;
                GlLogger.Instance.Error(ex.Message);
                GlLogger.Instance.LogInfo("[Error] Cannot found the image. path:" + filename);
                GlLogger.Instance.Error("Cannot found the image. path:" + filename);
            }
            finally
            {

            }
        }

        public string src
        {
            get
            {
                return _src;
            }
            set
            {
                if (_src != value)
                {
                    _src = value;
                    Dispose(true);
                    _loadTexture(_src, out _texture, out _bmp);
                    if (_bmp != null)
                    {
                        width = _bmp.Width;
                        height = _bmp.Height;
                    }
                }
            }
        }

        public override DrawObject2D Clone()
        {
            DrawImage dest = (DrawImage)base.Clone();
            dest.src = this.src;
            dest.LoadTexture(this._bmp);
            return dest;
        }

        public override JObject toJObject() 
        {
            dynamic jsonObject = base.toJObject();
            jsonObject.src = this.src;
            return jsonObject; 
        }

        public override void fromJObject(JObject jobj) 
        {
            base.fromJObject(jobj);
            if (jobj["src"] != null)
                this.src = (string)jobj["src"];
        }

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                }
                if (_bmp != null)
                    _bmp.Dispose();
                if (GraphicsContext.CurrentContext != null && _texture != -1)
                    GL.DeleteTexture(_texture);
                _disposed = true;
            }
            base.Dispose(disposing);
        }

        internal override void render(float deltaTime, bool hasTexture = false)
        {
            fillColor = true;
            if (_bmp != null)
            {
                GL.Enable(EnableCap.Texture2D);
                GL.BindTexture(TextureTarget.Texture2D, _texture);
                base.render(deltaTime, true);
                GL.Disable(EnableCap.Texture2D);
            }
            else
            {
                width = height = 1.0f;
                base.render(deltaTime);
            }
        }

    }
}
