﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wadraw
{
    public class Singleton<T> where T : class, new()
    {
        static private T _instance;

        static Singleton()
        {
            _instance = null;
        }

        static public T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new T();
                }
                return _instance;
            }
        }
    }
}
