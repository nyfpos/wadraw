﻿using Newtonsoft.Json.Linq;
using OpenTK;
using System;

namespace wadraw
{
    public class GlDeserializer : Singleton<GlDeserializer>
    {
        private Canvas _canvas;
        private bool _beInit;

        public GlDeserializer()
        {
            _canvas = null;
            _beInit = false;
        }

        public void Init(Canvas canvas)
        {
            _canvas = canvas;
            _beInit = true;
        }

        public void Deserialize(string jsonStr)
        {
            if (!_beInit || _canvas == null)
                throw new Exception("not initialize");
            JObject canvasJRoot = JObject.Parse(jsonStr);
            JObject drawobjs = canvasJRoot["drawobjs"] as JObject;
            JObject jcanvas = canvasJRoot["canvas"] as JObject; 

            try
            {
                foreach (var keypair in drawobjs)
                {
                    JObject jsonObject = drawobjs[keypair.Key] as JObject;
                    float modelSize = (float)jsonObject["modelSize"];
                    string className = (string)jsonObject["ClassName"];

                    DrawObject2D obj = null;
                    if (className == "wadraw.DrawImage")
                    {
                        string src = (string)jsonObject["src"];
                        obj = Activator.CreateInstance(Type.GetType(className), src) as DrawObject2D;
                    }
                    else if(className == "wadraw.DrawText")
                    {
                        obj = Activator.CreateInstance(Type.GetType(className)) as DrawObject2D;
                    }
                    else
                        obj = Activator.CreateInstance(Type.GetType(className), modelSize) as DrawObject2D;
                    _canvas.addObject(obj);
                    obj.canvasInfo = new CanvasInfo(_canvas.width, _canvas.height, _canvas.zoom);
                    obj.fromJObject(jsonObject);                  
                }
                if (jcanvas["width"]!= null)
                    _canvas.width = (int)jcanvas["width"];
                if (jcanvas["height"]!= null)
                    _canvas.height = (int)jcanvas["height"];
                if (jcanvas["color"] != null)
                    _canvas.color = (uint)jcanvas["color"];
            } catch(Exception ex)
            {                
                GlLogger.Instance.Info(ex.Message);
                GlLogger.Instance.Info(ex.StackTrace);
            }
        }

    }
}
