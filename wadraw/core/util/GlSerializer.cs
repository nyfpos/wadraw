﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wadraw
{
    public class GlSerializer : Singleton<GlSerializer>
    {
        private Canvas _canvas;
        private bool _beInit;

        public GlSerializer()
        {
            _canvas = null;
            _beInit = false;
        }


        public void Init(Canvas canvas)
        {
            _canvas = canvas;
            _beInit = true;
        }


        public string Serialize()
        {
            if (!_beInit || _canvas == null)
                throw new Exception("not initialize");

            JObject canvasJRoot = JObject.Parse(@"{canvas:{}, drawobjs:{}}");
            JObject drawobjs = canvasJRoot["drawobjs"] as JObject;
            DrawObject2D[] objs = _canvas.getAll2DObjects();
            for (int i = 0; i < objs.Length; i++)
            {
                DrawObject2D obj2d = objs[i];
                if (_canvas.drawObject2DSelectHelper.beHelperObjects(obj2d))
                    continue;
                JObject jobj = obj2d.toJObject();
                drawobjs.Add(obj2d.ID, jobj);
            }
            JObject jcanvas = canvasJRoot["canvas"] as JObject;
            jcanvas.Add("width", _canvas.width);
            jcanvas.Add("height", _canvas.height);
            jcanvas.Add("color", _canvas.color);
            return canvasJRoot.ToString(Formatting.None);
        }
        
    }
}
