﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace wadraw
{
    public class Gut
    {
        static public float Deg2Rad = (float)((Math.PI * 2) / 360);
        static public float Rad2Deg = (float)(360 / (Math.PI * 2));

        public static Quaternion ToQ(Vector3 v)
        {
            return ToQ(v.X, v.Y, v.Z);
        }

        public static Quaternion ToQ(float x_deg, float y_deg, float z_deg)
        {
            y_deg *= Deg2Rad;
            x_deg *= Deg2Rad;
            z_deg *= Deg2Rad;
            float rollOver2 = z_deg * 0.5f;
            float sinRollOver2 = (float)Math.Sin((double)rollOver2);
            float cosRollOver2 = (float)Math.Cos((double)rollOver2);
            float pitchOver2 = x_deg * 0.5f;
            float sinPitchOver2 = (float)Math.Sin((double)pitchOver2);
            float cosPitchOver2 = (float)Math.Cos((double)pitchOver2);
            float yawOver2 = y_deg * 0.5f;
            float sinYawOver2 = (float)Math.Sin((double)yawOver2);
            float cosYawOver2 = (float)Math.Cos((double)yawOver2);
            Quaternion result = new Quaternion();
            result.W = cosYawOver2 * cosPitchOver2 * cosRollOver2 + sinYawOver2 * sinPitchOver2 * sinRollOver2;
            result.X = cosYawOver2 * sinPitchOver2 * cosRollOver2 + sinYawOver2 * cosPitchOver2 * sinRollOver2;
            result.Y = sinYawOver2 * cosPitchOver2 * cosRollOver2 - cosYawOver2 * sinPitchOver2 * sinRollOver2;
            result.Z = cosYawOver2 * cosPitchOver2 * sinRollOver2 - sinYawOver2 * sinPitchOver2 * cosRollOver2;

            return result;
        }

        public static Vector3 FromQ2(Quaternion q1)
        {
            float sqw = q1.W * q1.W;
            float sqx = q1.X * q1.X;
            float sqy = q1.Y * q1.Y;
            float sqz = q1.Z * q1.Z;
            float unit = sqx + sqy + sqz + sqw; // if normalised is one, otherwise is correction factor
            float test = q1.X * q1.W - q1.Y * q1.Z;
            Vector3 v = new Vector3();

            if (test > 0.4995f * unit)
            { // singularity at north pole
                v.Y = (float)(2f * Math.Atan2(q1.Y, q1.X));
                v.X = (float)(Math.PI / 2);
                v.Z = 0;
                return NormalizeAngles(v * Rad2Deg);
            }
            if (test < -0.4995f * unit)
            { // singularity at south pole
                v.Y = (float)(-2f * Math.Atan2(q1.Y, q1.X));
                v.X = (float)(-Math.PI / 2);
                v.Z = 0;
                return NormalizeAngles(v * Rad2Deg);
            }
            Quaternion q = new Quaternion(q1.W, q1.Z, q1.X, q1.Y);
            v.Y = (float)Math.Atan2(2f * q.X * q.W + 2f * q.Y * q.Z, 1 - 2f * (q.Z * q.Z + q.W * q.W));     // Yaw
            v.X = (float)Math.Asin(2f * (q.X * q.Z - q.W * q.Y));                             // Pitch
            v.Z = (float)Math.Atan2(2f * q.X * q.Y + 2f * q.Z * q.W, 1 - 2f * (q.Y * q.Y + q.Z * q.Z));      // Roll
            return NormalizeAngles(v * Rad2Deg);
        }

        static public Vector3 NormalizeAngles(Vector3 angles)
        {
            angles.X = NormalizeAngle(angles.X);
            angles.Y = NormalizeAngle(angles.Y);
            angles.Z = NormalizeAngle(angles.Z);
            return angles;
        }

        static public float NormalizeAngle(float angle)
        {
            while (angle > 360)
                angle -= 360;
            while (angle < 0)
                angle += 360;
            return angle;
        }

        static public Matrix4 IdentityMatrix()
        {
            return new Matrix4(
                    new Vector4(1.0f, 0.0f, 0.0f, 0.0f),
                    new Vector4(0.0f, 1.0f, 0.0f, 0.0f),
                    new Vector4(0.0f, 0.0f, 1.0f, 0.0f),
                    new Vector4(0.0f, 0.0f, 0.0f, 1.0f)
                );
        }

        static public Matrix4 GutMatrixLookAtRH(ref Vector3 eye, ref Vector3 lookat, ref Vector3 up)
        {            
            Vector3 up_normalized = up.Normalized();
            Vector3 zaxis = (eye - lookat);
            zaxis.Normalize();
            Vector3 xaxis = Vector3.Cross(up_normalized, zaxis);
            Vector3 yaxis = Vector3.Cross(zaxis, xaxis);            
            Matrix4 matrix = IdentityMatrix();
            matrix.Column0 = new Vector4(xaxis, 1.0f);
            matrix.Column1 = new Vector4(yaxis, 1.0f);
            matrix.Column2 = new Vector4(zaxis, 1.0f);
            matrix.M41 = -Vector3.Dot(xaxis, eye);
            matrix.M42 = -Vector3.Dot(yaxis, eye);
            matrix.M43 = -Vector3.Dot(zaxis, eye);
            return matrix;
        }

        static public double Cotan(float radian)
        {
            return 1 / Math.Tan(radian);
        }

        static public Matrix4 GutMatrixPerspectiveRH_OpenGL(float fovy, float aspect, float z_near, float z_far)
        {
            Matrix4 matrix = IdentityMatrix();

	        float fovy_radian = Gut.Deg2Rad *fovy;
            float yscale = (float)Cotan(fovy_radian / 2.0f);
	        float xscale = yscale * aspect;

	        matrix.M11 = xscale;
	        matrix.M22 = yscale;
	        matrix.M33 = (z_far + z_near)/(z_near - z_far);
	        matrix.M34 = -1.0f;
	        matrix.M43 = 2.0f * z_far * z_near / (z_near - z_far);
	        matrix.M44 = 0.0f;
            return matrix;
        }

        static public Matrix4 GutMatrixOrthoRH_OpenGL(float w, float h, float z_near, float z_far)
        {
            Matrix4 matrix = IdentityMatrix();
            matrix.M11 = 2.0f / w;
            matrix.M22 = 2.0f / h;
            matrix.M33 = 2.0f / (z_near - z_far);
            matrix.M43 = (z_far + z_near) / (z_near - z_far);
            matrix.M44 = 1.0f;
            return matrix;
        }

        static public Vector4 MulVector4Matrix4(ref Vector4 v, ref Matrix4 m)
        {
            Vector4 result = new Vector4();
            result.X = v.X * m.M11 + v.Y * m.M21 + v.Z * m.M31 + v.W * m.M41;
            result.Y = v.X * m.M12 + v.Y * m.M22 + v.Z * m.M32 + v.W * m.M42;
            result.Z = v.X * m.M13 + v.Y * m.M23 + v.Z * m.M33 + v.W * m.M43;
            result.W = v.X * m.M14 + v.Y * m.M24 + v.Z * m.M34 + v.W * m.M44;
            return result;
        }

        static public Vector4 MulMatrix4Vector4(ref Matrix4 m, ref Vector4 v)
        {
            Vector4 result;
            result.X = Vector4.Dot(v, m.Row0);
            result.Y = Vector4.Dot(v, m.Row1);
            result.Z = Vector4.Dot(v, m.Row2);
            result.W = Vector4.Dot(v, m.Row3);
            return result;
        }
        /*
            inline Vector4CPU operator*(Vector4CPU &v, Matrix4x4CPU &m)
            {
	            Vector4CPU result;

	            result.x = v.x*m.m_00 + v.y*m.m_10 + v.z*m.m_20 + v.w*m.m_30;
	            result.y = v.x*m.m_01 + v.y*m.m_11 + v.z*m.m_21 + v.w*m.m_31;
	            result.z = v.x*m.m_02 + v.y*m.m_12 + v.z*m.m_22 + v.w*m.m_32;
	            result.w = v.x*m.m_03 + v.y*m.m_13 + v.z*m.m_23 + v.w*m.m_33;

	            return result;
            }

            inline Vector4CPU operator*(Matrix4x4CPU &matrix, Vector4CPU &vec)
            {
	            Vector4CPU result;

	            result[0] = VectorDot(vec, matrix[0]).GetX();
	            result[1] = VectorDot(vec, matrix[1]).GetX();
	            result[2] = VectorDot(vec, matrix[2]).GetX();
	            result[3] = VectorDot(vec, matrix[3]).GetX();

	            return result;
            }
         */
    }
}
