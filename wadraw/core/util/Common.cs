﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wadraw
{
    public enum UpdateBufferType
    {
        Texture,
        Color,
        Position,
        Indices
    }

    public enum DrawBufferStatus
    {
        None,
        Loaded,
        FailedLoad
    }

    public enum LineType
    {
        None,
        DashedLine,
        DashedDotedLine
    }

    public struct Mesh
    {
        public float[] texCoords;
        public float[] position;
        public byte[] color;
    }

    public class Transform
    {
        public bool beDirty { get; internal set; }
        private Vector3 _pos;
        public Vector3 position
        {
            get { return _pos; }
            set { _pos = value; beDirty = true; }
        }
        private Quaternion _rot;
        public Quaternion rotation
        {
            get { return _rot; }
            set { _rot = value; beDirty = true; }
        }
        private Vector3 _scal;
        public Vector3 scale
        {
            get { return _scal; }
            set { _scal = value; beDirty = true; }
        }

        public void clear()
        {
            position = Vector3.Zero;
            rotation = new Quaternion(Vector3.Zero, 1);
            scale = Vector3.One;
            beDirty = true;
        }

        public void clearScale()
        {
            scale = Vector3.One;
            beDirty = true;
        }

        public void clearRotation()
        {
            rotation = new Quaternion(Vector3.Zero, 1);
            beDirty = true;
        }

        public void clearPosition()
        {
            position = Vector3.Zero;
            beDirty = true;
        }

        public Transform()
        {
            position = Vector3.Zero;
            rotation = new Quaternion(Vector3.Zero, 1);
            scale = Vector3.One;
            beDirty = false;
        }
    }

    struct MatrixConst
    {
        static readonly public string ProjectMatrix = "ProjectMatrix";
        static readonly public string ViewMatrix = "ViewMatrix";
    }

    public class Point
    {
        public float x { get; set; }
        public float y { get; set; }

        public Point(float x_, float y_)
        {
            x = x_;
            y = y_;
        }

        public Point()
        {
            x = y = 0.0f;
        }
    }

    public struct CanvasInfo
    {
        public int width;
        public int height;
        public float zoom;
        public CanvasInfo(int w, int h, float zoom_)
        {
            width = w;
            height = h;
            zoom = zoom_;
        }
    }

    public enum CanvasUpdateMode
    {
        Normal,
        Timer
    }
}
