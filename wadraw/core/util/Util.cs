﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace wadraw
{


    public class Util
    {
        static public GLControl currentControl = null;

        static public Canvas currentCanvas = null;


        static internal int getCanvasWidth() { return currentCanvas != null ? currentCanvas.width : 0; }

        static internal int getCanvasHeight() { return currentCanvas != null ? currentCanvas.height : 0; }


        static public Point CursorPos()
        {
         //   return new Point(Cursor.Position.X, Cursor.Position.Y);
            if (currentControl != null)
            {
                System.Drawing.Point p = currentControl.PointToClient(Cursor.Position); 
                return new Point(p.X , p.Y );
            }
            return null;
        }

        static public Vector3 ScreenToViewportPointFast(int screenWidth, int screenHeight, Point p)
        {
            return new Vector3(
               ScreenToViewportPointFast_X(screenWidth, p.x),
               ScreenToViewportPointFast_Y(screenHeight, p.y), 
               0);        
        }

        static public Point ViewportToScreenPointFast(int screenWidth, int screenHeight, Vector3 v)
        {
            return new Point(ViewportToScreenPointFast_X(screenWidth, v.X),
                             ViewportToScreenPointFast_Y(screenHeight, v.Y));
        }

        static public float ScreenToViewportPointFast_X(int screenWidth, float x_)
        {
            float x = x_;
            x -= (screenWidth >> 1);
            return x;
        }

        static public float ScreenToViewportPointFast_Y(int screenHeight, float y_)
        {
            float y = y_;
            y -= (screenHeight >> 1);
            return -y; // the y of screen point against y of viewport                                       
        }

        static public float ViewportToScreenPointFast_X(int screenWidth, float x_)
        {
            float x = x_;
            x += (screenWidth >> 1);            
            return x;
        }

        static public float ViewportToScreenPointFast_Y(int screenHeight, float y_)
        {
            float y = y_;
            y -= (screenHeight >> 1);                        
            return -y; // the y of screen point against y of viewport 
        }

        /****
         *  note: in 3D -space 
         *  leftTop:     (minX, maxY)
         *  rightBottom: (maxX, minY)
         */
        static public void GetLeftTopRightBottom3D(float[] position, out Vector4 leftTopRightBottom)
        {
            leftTopRightBottom = new Vector4();
            Point minXY, maxXY;
            GetMinMaxPoint(position, out minXY, out maxXY);            
            leftTopRightBottom[0] = minXY.x;
            leftTopRightBottom[1] = maxXY.y;
            leftTopRightBottom[2] = maxXY.x;
            leftTopRightBottom[3] = minXY.y;
            /*
            GlLogger.Instance.Debug("-----------------------");
            GlLogger.Instance.Debug(String.Format("leftTop({0},{1}) rightBottom({2},{3})", leftTopRightBottom[0], leftTopRightBottom[1], leftTopRightBottom[2], leftTopRightBottom[3]));
            GlLogger.Instance.Debug("-----------------------");*/
        }

        static public void GetMinMaxPoint(float[] position, out Point minXY, out Point maxXY)
        {
            minXY = new Point(float.MaxValue, float.MaxValue);
            maxXY = new Point(float.MinValue, float.MinValue);
            const int dimension = 3;
            for (int i = 0; i < position.Length; i++)
            {                
                if (i % dimension == 0)
                {
                    float x = position[i];
                    float y = position[i+1];
                    float z = position[i+2];
            //        GlLogger.Instance.Debug(String.Format("{0},{1},{2}", x, y, z));
                    if (x < minXY.x)
                        minXY.x = x;
                    if (y < minXY.y)
                        minXY.y = y;
                    if (x > maxXY.x)
                        maxXY.x = x;
                    if (y > maxXY.y)
                        maxXY.y = y;
                }
            }
            /*
            GlLogger.Instance.Debug("-----------------------");
            GlLogger.Instance.Debug(String.Format("minXY({0},{1}) maxXY({2},{3})", minXY.x, minXY.y, maxXY.x, maxXY.y));
            GlLogger.Instance.Debug("-----------------------");*/
        }

        static public float[] TransformEachPoint(float[] points, ref Matrix4 transform, int pointDim=3)
        {
            const int maxVectorDim = 4;

            if (points == null || pointDim > maxVectorDim)
                return null;
            float[] result = new float[points.Length];
            for (int i = 0; i < points.Length; i++ )
            {
                if (i%pointDim == 0)
                {
                    Vector4 v =Vector4.Zero;                    
                    for (int j = 0; j < pointDim; j++)
                    {
                        v[j] = points[i + j];
                    }
                    if (pointDim < maxVectorDim)
                        v[3] = 1;
                    Vector4 tmp = Gut.MulVector4Matrix4(ref v, ref transform);
                    for (int j = 0; j < pointDim; j++)
                        result[i + j] = tmp[j];
                }                
            }
            return result;
        }

        /*
        0 1 2 3
        X Y A B
        0  1  2  3
        X1 Y1 A1 B1
        */
        static public bool IntersectionRect(Vector4 rect1, Vector4 rect2)
        {
            if (rect1[2] < rect2[0] ||
                rect2[2] < rect1[0] ||
                rect1[3] < rect2[1] ||
                rect2[3] < rect1[1])
                return false;
            return true;
        }

        static public bool OutOfBoundary(Vector4 rect1, Vector4 rect2, float threshold=1.0f)
        {
            if (Math.Abs(rect1[0] - rect2[0]) > threshold ||
                Math.Abs(rect1[1] - rect2[1]) > threshold ||
                Math.Abs(rect1[2] - rect2[2]) > threshold ||
                Math.Abs(rect1[3] - rect2[3]) > threshold)
                return true;
            return false;
        }

        static public bool IntersectionDrawObject2D(DrawObject2D obj, Vector4 rect)
        {
            float minX = rect[0];
            float minY = rect[1];
            float maxX = rect[2];
            float maxY = rect[3];

            if (obj is Line)
            {
                return (
                    obj.leftTop.x > minX &&
                    obj.leftTop.y >= minY &&
                    obj.rightBottom.x < maxX &&
                    obj.rightBottom.y <= maxY
                );
            }
            else
            {
                return (
                    obj.leftTop.x > minX &&
                    obj.leftTop.y > minY &&
                    obj.rightBottom.x < maxX &&
                    obj.rightBottom.y < maxY
                );
            }
        }

        static public bool IntersectionDrawObject2D(DrawObject2D obj, float px, float py)
        {
            if (obj is Line)
            {
                return (
                px > obj.leftTop.x &&
                px < obj.rightBottom.x &&
                py >= (int)(obj.leftTop.y) &&
                py <= (int)(obj.rightBottom.y));
            }
            else
            {
                return (
                px > obj.leftTop.x &&
                px < obj.rightBottom.x &&
                py > obj.leftTop.y &&
                py < obj.rightBottom.y);
            }
        }

        public static Brush HexColorToBrush(uint color, float alpha)
        {
            uint tmp = 0;
            tmp = color & 0x00FF0000;
            byte r = (byte)(tmp >> 16);
            tmp = color & 0x0000FF00;
            byte g = (byte)(tmp >> 8);
            tmp = color & 0x000000FF;
            byte b = (byte)tmp;
            return new SolidBrush(Color.FromArgb((int)(255 *alpha), (int)r, (int)g, (int)b));
        }

        public static Color HexColor(uint color, float alpha)
        {
            uint tmp = 0;
            tmp = color & 0x00FF0000;
            byte r = (byte)(tmp >> 16);
            tmp = color & 0x0000FF00;
            byte g = (byte)(tmp >> 8);
            tmp = color & 0x000000FF;
            byte b = (byte)tmp;
            return Color.FromArgb((int)(255 * alpha), (int)r, (int)g, (int)b);
        }

        /*
        static public int Div(int i, int size)
        {
            int remain = i %size;
            return (int)((i - remain) / size);
        }
        */
    }
}
