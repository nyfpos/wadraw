﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace wadraw
{
    public class SerializableObject
    {
        private DrawObject2D _obj;

        public SerializableObject(DrawObject2D obj)
        {
            _obj = obj;
        }

        public string toJsonString()
        {
            dynamic jsonObject = toJObject();
            return jsonObject.ToString(Formatting.None);
        }

        public void fromJObject(JObject jsonObject)
        {
            if (jsonObject["ID"] != null)
                _obj.ID = (string)jsonObject["ID"];
            if (jsonObject["name"] != null)
                _obj.name = (string)jsonObject["name"];
            if (jsonObject["color"] != null)
                _obj.color = (uint)jsonObject["color"];
            if (jsonObject["alpha"] != null)
                _obj.alpha = (float)jsonObject["alpha"];
            if (jsonObject["fillColor"] != null)
                _obj.fillColor = (bool)jsonObject["fillColor"];
            if (jsonObject["x"] != null)
                _obj.x = (float)jsonObject["x"];
            if (jsonObject["y"] != null)
                _obj.y = (float)jsonObject["y"];
            if (jsonObject["z"] != null)
                _obj.z = (float)jsonObject["z"];
            if (jsonObject["scaleX"] != null)
                _obj.scaleX = (float)jsonObject["scaleX"];
            if (jsonObject["scaleY"] != null)
                _obj.scaleY = (float)jsonObject["scaleY"];
            if (jsonObject["rotateZ"] != null)
                _obj.rotateAroundZ((float)jsonObject["rotateZ"]);
            if (jsonObject["modelSize"] != null)
                _obj.modelSize = (float)jsonObject["modelSize"];
            if (jsonObject["visible"] != null)
                _obj.visible = (bool)jsonObject["visible"];
        }

        public JObject toJObject()
        {
            dynamic jsonObject = new JObject();
            jsonObject.ID = _obj.ID;
            jsonObject.name = _obj.name;
            jsonObject.ClassName = _obj.GetType().ToString();
            jsonObject.color = _obj.color;
            jsonObject.alpha = _obj.alpha;
            jsonObject.fillColor = _obj.fillColor;
            jsonObject.x = _obj.x;
            jsonObject.y = _obj.y;
            jsonObject.z = _obj.z;
            jsonObject.scaleX = _obj.scaleX;
            jsonObject.scaleY = _obj.scaleY;
            jsonObject.rotateZ = _obj.getRotatedDegreeZ();
            jsonObject.modelSize = _obj.modelSize;
            jsonObject.visible = _obj.visible;
            return jsonObject;
        }

        public void loadFromJson(string jsonStr)
        {            
            fromJObject(JObject.Parse(jsonStr));
        }             

    }
}
