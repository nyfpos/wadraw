﻿using Newtonsoft.Json.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

namespace wadraw
{
    public class Canvas
    {
        private GLControl _glControl;

        internal List<DrawObject> _drawObjects;

        private Stopwatch _sw1;
        private Stopwatch _sw2;

        private float _frameRate;

        private float _during;

        private Camera _camera;
        private Matrix4 _viewMatrix;
        private Matrix4 _projectMatrix;

        private bool _beLoaded;
        
        private float[] _sampleFPS;
        private int _sampleFPSIndex;

        public Action<float> OnUpdate;

        public float LastValZ;

        private DrawObject2DEventHelper _2DEventHelper;

        private uint _color;
        private float _alpha;

        private DrawObject2DSelectHelper _2DSelectHelper;

        private float _zoom;
        private Vector2 _zoomXY;

        private Timer _timer;

        private bool _beStart;

        private List<DrawObject> _deleteObjTmp;
        private List<DrawObject2D> _all2DObjTmp;

        private CanvasUpdateMode _updateMode;
        public CanvasUpdateMode updateMode
        {
            get
            {
                return _updateMode;
            }
            set
            {
                if (value != _updateMode)
                {                    
                    _updateMode = value;
                    if (_beStart)
                    {
                        stop();
                        start();
                    }
                }
            }
        }

        public Canvas(GLControl glControl)
        {
            const int MAX_SAMPLE_FPS = 100;

            _glControl = glControl;
            _drawObjects = new List<DrawObject>();
            _sw1 = new Stopwatch();
            _sw2 = new Stopwatch();
            _frameRate = 1000.0f / 60.0f;
            _during = 0.0f;
            _sampleFPS = new float[MAX_SAMPLE_FPS];
            _sampleFPSIndex = 0;
            _camera = null;
            _beLoaded = false;
            OnUpdate = null;
            LastValZ = 0.0f;
            autoArrangeZByAddedOrder = false;
            _antiAliased = false;
            _beBlend = false;
            _2DEventHelper = new DrawObject2DEventHelper(this);
            _2DSelectHelper = new DrawObject2DSelectHelper(this);
            _alpha = 1.0f;
            _zoom = 1.0f;
            _zoomXY = Vector2.Zero;            

            this.width = _glControl.Width;
            this.height = _glControl.Height;
            
            this._addEventListeners();

            Util.currentControl = glControl;
            Util.currentCanvas = this;
            GlDeserializer.Instance.Init(this);
            GlSerializer.Instance.Init(this);

            _timer = new Timer();
            _timer.Interval = 1;

            _updateMode = CanvasUpdateMode.Normal;

            _beStart = false;

            _deleteObjTmp = new List<DrawObject>();
            _all2DObjTmp = new List<DrawObject2D>();
        }

        private void _addEventListeners()
        {
            _glControl.Paint += OnPaint;
            _glControl.Load += OnLoaded;
            _glControl.Resize += OnResize;
            _glControl.MouseDown += OnMouseDown;
            _glControl.MouseUp += OnMouseUp;
            _glControl.MouseMove += OnMouseMove;
            _glControl.MouseHover += OnMouseHover;
            _glControl.MouseEnter += OnMouseEnter;
            _glControl.MouseLeave += OnMouseLeave;
            _glControl.KeyDown += OnKeyDown;
            _glControl.KeyUp += OnKeyUp;
        }

        public bool beSelect
        {
            get
            {
                return _2DSelectHelper.beStart;
            }
            set
            {
                _2DSelectHelper.beStart = value;
            }
        }
        
        public float alpha
        {
            get
            {
                return _alpha;
            }
            set
            {
                _alpha = value;
            }
        }

        public uint color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value & 0x00FFFFFF;
              /* uint tmp = (byte)(0xFF * alpha);
                tmp = tmp << 24;
                _color = _color | tmp;*/
            }
        }

        private  Vector4 _getRGBA ()
        {
            Vector4 rtn = Vector4.Zero;
            uint tmp = _color & 0x00FF0000;
            rtn[0] = (byte)(tmp >> 16);
            tmp = _color & 0x0000FF00;
            rtn[1] = (byte)(tmp >> 8);
            rtn[2] = (byte)(_color & 0x000000FF);
           // tmp = _color & 0xFF000000;
            rtn[3] = _alpha;//(byte)(tmp >> 24);

            rtn[0] /= 0xFF;
            rtn[1] /= 0xFF;
            rtn[2] /= 0xFF;
            return rtn;
        }

        private bool _beBlend;
        public bool beBlend
        {
            get
            {
                return _beBlend;
            }
            set
            {
                _beBlend = value;
                if (_beBlend)
                {
                    GL.Enable(EnableCap.Blend);
                    GL.BlendFuncSeparate(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha, BlendingFactorSrc.Zero, BlendingFactorDest.One);
                }
                else
                {
                    GL.Disable(EnableCap.Blend);
                }
            }
        }

        public DrawObject2DEventHelper drawObject2DEventHelper
        {
            get
            {
                return _2DEventHelper;
            }
        }

        public DrawObject2DSelectHelper drawObject2DSelectHelper
        {
            get
            {
                return _2DSelectHelper;
            }
        }

        public bool beStart2DEvent
        {
            get
            {
                return _2DEventHelper.beStart;
            } 
            set
            {
                _2DEventHelper.beStart = value;
            }
        }

        public float frameRate
        {
            get
            {
                return _frameRate;
            }
            set
            {
                _frameRate = value;
            }
        }

        public int width
        {
            get
            {
                return _glControl.Width;
            }
            set
            {
                _glControl.Width = value;
                for (int i = 0; i < _drawObjects.Count; i++)
                {
                    _drawObjects[i].canvasInfo = new CanvasInfo( _glControl.Width, _glControl.Height, zoom);
                }                
            }
        }

        public int height
        {
            get
            {
                return _glControl.Height;
            }
            set
            {
                _glControl.Height = value;
                for (int i = 0; i < _drawObjects.Count; i++)
                {
                    _drawObjects[i].canvasInfo = new CanvasInfo(_glControl.Width, _glControl.Height, zoom);
                }                
            }
        }

        public bool autoArrangeZByAddedOrder
        {
            get;
            set;
        }

        public void addObject(DrawObject obj)
        {
            DrawObject2D obj2d = obj as DrawObject2D;
            JObject jobj = null;
            if (obj2d != null)
                jobj = obj2d.toJObject();
            obj.canvasInfo = new CanvasInfo(_glControl.Width, _glControl.Height, zoom);
            if (_beLoaded)
            {
                obj.bindMatrix(ref _viewMatrix, ref _projectMatrix);
            }
            if (autoArrangeZByAddedOrder)
            {
                obj.z = LastValZ;
                LastValZ += Common2D.CommonLayerSetp;
            }
            _drawObjects.Add(obj);
                        
            if (obj2d!=null)
            {
                if (jobj != null)
                {
                    obj2d.transform.clear();
                    obj2d.fromJObject(jobj);
                }
                obj2d.Emit(null, obj2d, EventType.AddedCanvas, new EventArgs());
            }            
        }

        public void clearAllObject()
        {
            
            for (int i = 0; i < _drawObjects.Count; i++)
            {
                var obj = _drawObjects[i] as DrawObject2D;
                if (obj != null && _2DSelectHelper.beHelperObjects(obj))
                    continue;
                _drawObjects[i].Dispose();
                _deleteObjTmp.Add(_drawObjects[i]);
            }            
            while(_deleteObjTmp.Count> 0)
            {
                _drawObjects.Remove(_deleteObjTmp[0]);
                _deleteObjTmp.RemoveAt(0);
            }            
        }

        public void removeObject(DrawObject obj)
        {
            _drawObjects.Remove(obj);
            obj.Emit(null, obj, EventType.RemovedCanvas, new EventArgs());
        }

        public void removeAndDisposeObject(DrawObject obj)
        {
            if (_drawObjects.Remove(obj))
            {
                obj.Dispose();
            }
        }

        /*
        public DrawObject[] getAllObjects()
        {            
            return _drawObjects.ToArray();
        }

        public DrawObject[] getObjectByRange()
        {
            return new DrawObject[0];
        }
        */

        public DrawObject2D[] getAll2DObjects()
        {
            _all2DObjTmp.Clear();
            for (int i = 0; i < _drawObjects.Count; i++)
            {
                var obj = _drawObjects[i] as DrawObject2D;
                if (obj != null && _2DSelectHelper.beHelperObjects(obj))
                    continue;
                _all2DObjTmp.Add(obj);
            }
            return _all2DObjTmp.ToArray();
        }

        private bool _antiAliased;
        public bool antiAliased
        {
            get
            {
                return _antiAliased;
            }
            set
            {
                _antiAliased = value;
                if (_beLoaded)
                {
                    if (_antiAliased)
                    {
                        GL.Enable(EnableCap.PolygonSmooth);
                        GL.Enable(EnableCap.LineSmooth);
                    }
                    else
                    {
                        GL.Disable(EnableCap.PolygonSmooth);
                        GL.Disable(EnableCap.LineSmooth);
                    }
                }
            }
        }

        public bool bePerspective { get; set; }

        public float zoom
        {
            get
            {
                return _zoom;
            }
            set
            {
                if (_zoom != value)
                {
                    _zoom = value;
                    OnResize(this, new EventArgs());
                }
            }
        }

        public Vector2 zoomXY
        {
            get
            {
                return _zoomXY;
            }
            set
            {
                if (value != _zoomXY)
                {
                    _zoomXY = value;
                    OnResize(this, new EventArgs());
                }
            }
        }

        private bool _initResourceOpenGL()
        {           
            float aspect = (float)width / (float)height;
            _camera = new Camera();
            _camera.lookat = new Vector3(0.0f, 0.0f, 0.0f);
            _camera.up = new Vector3(0.0f, 1.0f, 0.0f);

            GL.Viewport(0, 0, width, height);

            GL.MatrixMode(MatrixMode.Projection);   
            if (bePerspective)
            {
                _camera.eye = new Vector3(0.0f, 0.0f, 10.0f);
                _projectMatrix = Gut.GutMatrixPerspectiveRH_OpenGL(90.0f, aspect, 0.1f, 100.0f);
                GL.LoadMatrix(ref _projectMatrix);
            }
            else
            {
                _camera.eye = new Vector3(0.0f, 0.0f, 1.0f);
                _projectMatrix = Gut.GutMatrixOrthoRH_OpenGL(width * zoom, height * zoom, -1.0f, 1.0f);
                GL.LoadMatrix(ref _projectMatrix);              
            }
           // GL.LoadMatrix(ref _projectMatrix);
            beBlend = true;
//            GL.Enable(EnableCap.DepthTest);
          //  GL.Enable(EnableCap.CullFace);                  
            //GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            _viewMatrix = Gut.GutMatrixLookAtRH(ref _camera.eye, ref _camera.lookat, ref _camera.up);
            if (zoomXY != Vector2.Zero)
                _viewMatrix = Matrix4.CreateTranslation(new Vector3(zoomXY.X, zoomXY.Y, 0.0f)) * _viewMatrix;
            return true;
        }

        public Action<object, EventArgs> OnCanvasLoaded;
        private void OnLoaded(object sender, EventArgs e)
        {
            _initResourceOpenGL();
            for (var i = 0; i < _drawObjects.Count; i ++ )
            {
                _drawObjects[i].bindMatrix(ref _viewMatrix, ref _projectMatrix);
            }            
            Application.ApplicationExit += OnApplicationExit;
            _beLoaded = true;

            start();

            if (OnCanvasLoaded != null)
                OnCanvasLoaded(sender, e);
        }

        public void start()
        {            
            if (updateMode == CanvasUpdateMode.Normal)
            {
                stop();
                _beStart = true;
                Application.Idle += _normalUpdate;
            } else if (updateMode == CanvasUpdateMode.Timer)
            {
                stop();
                _beStart = true;                
                _timer.Tick += _timerUpdate;
                _timer.Start();
            }
        }

        public void stop()
        {
            _beStart = false;
            Application.Idle -= _normalUpdate;
            _timer.Tick -= _timerUpdate;
            _timer.Stop();
        }

        private void OnApplicationExit(object sender, EventArgs e)
        {
            for (int i =0; i < _drawObjects.Count; i++)
            {
                _drawObjects[i].Dispose();
            }
            _drawObjects.Clear();
        }

        public Action<object, EventArgs> OnCanvasResize;
        private void OnResize(object sender, EventArgs e)
        {            
            _initResourceOpenGL();            
            //          MessageBox.Show("w:" + width + " h:" + height);

            GlLogger.Instance.LogInfo("(canvas) width:" + width, "height:" + height);

            for (int i = 0; i < _drawObjects.Count; i++)
            {
                DrawObject obj = _drawObjects[i];
                obj.canvasInfo = new CanvasInfo(_glControl.Width, _glControl.Height, zoom);
                obj.bindMatrix(ref _viewMatrix, ref _projectMatrix);                
                
                DrawObject2D obj2d = _drawObjects[i] as DrawObject2D;
                if (obj2d != null)
                {                    
                    obj2d.x = obj2d._tmpXY[0];
                    obj2d.y = obj2d._tmpXY[1];
                    obj2d._resetRange();
                }
            }

            if (OnCanvasResize != null)
                OnCanvasResize(sender, e);
        }

        public Action<object, PaintEventArgs> OnCanvasPaint;
        private void OnPaint(object sender, PaintEventArgs e)
        {
            if (OnCanvasPaint != null)
                OnCanvasPaint(sender, e);
        }

        public Action<object, MouseEventArgs> OnCanvasMouseDown;
        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            if (OnCanvasMouseDown != null)
                OnCanvasMouseDown(sender, e);
        }

        public Action<object, MouseEventArgs> OnCanvasMouseUp;
        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            if (OnCanvasMouseUp != null)
                OnCanvasMouseUp(sender, e);
        }

        public Action<object, EventArgs> OnCanvasMouseHover;
        private void OnMouseHover(object sender, EventArgs e)
        {                        
            if (OnCanvasMouseHover != null)
                OnCanvasMouseHover(sender, e);
        }

        public Action<object, EventArgs> OnCanvasMouseLeave;
        private void OnMouseLeave(object sender, EventArgs e)
        {                        
            if (OnCanvasMouseLeave!=null)
            {
                OnCanvasMouseLeave(sender, e);
            }
        }

        public Action<object, EventArgs> OnCanvasMouseEnter;
        private void OnMouseEnter(object sender, EventArgs e)
        {                        
            if (OnCanvasMouseEnter != null)
                OnCanvasMouseEnter(sender, e);
        }

        public Action<object, MouseEventArgs> OnCanvasMouseMove;
        private void OnMouseMove(object sender, MouseEventArgs e)
        {                        
            if (OnCanvasMouseMove != null)
                OnCanvasMouseMove(sender, e);
        }

        public Action<object, KeyEventArgs> OnCanvasKeyDown;
        private void OnKeyDown(object sender, KeyEventArgs e)
        {                        
            if (OnCanvasKeyDown != null)
                OnCanvasKeyDown(sender, e);
        }

        public Action<object, KeyEventArgs> OnCanvasKeyUp;
        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            if (OnCanvasKeyUp != null)
                OnCanvasKeyUp(sender, e);            
        }

        public float averageTimeSlice
        {
            get
            {
                int cnt =  0;
                float sum = 0;
                for (var i = 0; i < _sampleFPS.Length; i++)
                {
                    if (_sampleFPS[i] > 0)
                    {
                        sum += _sampleFPS[i];
                        cnt++;
                    }
                }

                return sum / cnt;
            }
        }

        public int fps
        {
            get
            {
                return (int)(Math.Ceiling(1000 / averageTimeSlice));
            }
        }


        private float _ComputeTimeSlice(Stopwatch sw)
        {
            sw.Stop();
            double timeslice = sw.Elapsed.TotalMilliseconds;
            sw.Reset();
            sw.Start();
            return (float)timeslice;
        }

        private void _update()
        {
            _during += _ComputeTimeSlice(_sw1);
            if (_during >= _frameRate)
            {
                _sampleFPS[_sampleFPSIndex++] = _during;
                _sampleFPSIndex %= _sampleFPS.Length;
                _during -= _frameRate;
                _Render(_ComputeTimeSlice(_sw2) / 1000.0f);
            }
        }

        private void _normalUpdate(object sender, EventArgs e)
        {
            while (_glControl.IsIdle && _beStart)
            {
                _update();
                System.Threading.Thread.Sleep(1);
            }
        }

        private void _timerUpdate(object sender, EventArgs e)
        {
            if (_glControl.IsIdle && _beStart)
            {
                _update();
            }
        }

        private int _SortRule(DrawObject obj1, DrawObject obj2)
        {
            return obj1.z.CompareTo(obj2.z);
        }

        private void _Render(float deltaTime)
        {
            if (OnUpdate != null)
            {
                OnUpdate(deltaTime);
            }

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            _drawObjects.Sort(_SortRule);
            Vector4 bgndColor = _getRGBA();
            GL.ClearColor(bgndColor[0], bgndColor[1], bgndColor[2], bgndColor[3]);
            for (int i = 0; i < _drawObjects.Count; i++)
            {
                if (_drawObjects[i].visible)
                {
                    _drawObjects[i].updateBeforeRender();
                    _drawObjects[i].render(deltaTime);
                }
            }
            _glControl.SwapBuffers();
        }

    }
}
