﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wadraw
{
    public enum EventType
    {
        /*
        OnLoaded = 0,
        OnPain,
        OnResize,
        OnMouseDown,
        OnMouseUp,
        OnMouseMove,
        OnMouseHover,
        OnRender,
        RenderAddDrawObject,
        RenderReadyViewProject,
         */
        Paint = 0,
        CanvasMouseDown,
        CanvasMouseUp,
        CanvasMouseMove,
        CanvasKeyDown,
        CanvasKeyUp,
        CanvasMouseHover,
        CanvasMouseLeave,
        CanvasMouseEnter,    

        AddedCanvas,
        RemovedCanvas,

        MouseDown,
        MouseUp,
        MouseHover,
        MouseLeave,

        SelectBegin,
        SelectEnd,
        SelectMoveBegin,
        SelectMoveEnd,
        SelectMoving,
        SelectScaling,
    }
}
