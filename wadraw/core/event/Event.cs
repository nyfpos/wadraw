﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace wadraw
{
    public class GlEvent
    {
        public EventType type { get; private set; }
        public object target { get; private set; } // who is watching this event...

        /*
   //    public EventArgs eventArgs { get; set; }
           private object _sender;
           internal void SetSender(object sender)
           {
               _sender = sender;
           }
           public object sender
           {
               get
               {
                   return _sender;
               }
           }       
           public object data
           {
               internal set;
               get;
           }
        */

        internal Action<object, DrawObject, EventArgs> callback;
        public GlEvent(EventType type_, object targe_)
        {                 
            type = type_;
            target = targe_;
        }
    }

    public class GlEventListener
    {
        /*
        static private Dictionary<EventType, List<GlEvent>> _events; // event_type, events
        static GlEventListener()
        {
            _events = new Dictionary<EventType, List<GlEvent>>();
        }
        static private void _Each(EventType type, Action<GlEvent> cb)
        {            
            List<GlEvent> evts = null;
            if (!_events.TryGetValue(type, out evts))
                return;
            for (var i = 0; i < evts.Count; i++)
            {
                cb(evts[i]);
            }
        }
        static public void Emit(object sender, EventType type, EventArgs args, object data_ = null)
        {
            _Each(type, (evt) =>
            {

                if (evt.callback != null)
                {
                    try
                    {
                        evt.SetSender( sender );
                        evt.eventArgs = args;
                        evt.data = data_;
                        evt.callback(evt);
                    }
                    catch (Exception ex)
                    {
                        GlLogger.Instance.Error(ex.Source+"\n"+ex.Message +"\n"+ex.StackTrace);
                    }
                }

            });
        }
        */

        static private Dictionary<EventType, Dictionary<string,HashSet<Action<object, DrawObject, EventArgs>>>> _eventCount; // event_type, counts
        static GlEventListener()
        {
            _eventCount = new Dictionary<EventType, Dictionary<string, HashSet<Action<object, DrawObject, EventArgs>>>>();
        }

        public static int CountEvents(EventType type)
        {
            Dictionary<string, HashSet<Action<object, DrawObject, EventArgs>>> dict;
            if (_eventCount.TryGetValue(type, out dict))
            {
                int sum = 0;
                foreach (var pair in dict)
                {
                    sum += dict[pair.Key].Count;
                }
                return sum;
            }
            return 0;
        }

        private Dictionary<EventType, List<GlEvent>> _events;

        public string BaseID;

        public GlEventListener()
        {
            _events = new Dictionary<EventType, List<GlEvent>>();
            BaseID = Guid.NewGuid().ToString();
        }

        public void Emit(object sender, DrawObject dwobj, EventType type, EventArgs args)
        {
            List<GlEvent> evts = null;
            if (!_events.TryGetValue(type, out evts))
                return;
            for (var i = 0; i < evts.Count; i++)            
            {
                GlEvent evt = evts[i];
                if (evt.callback != null)
                {
                    try
                    {                                                                                      
                       evt.callback(sender, dwobj, args);                                               
                    }
                    catch (Exception ex)
                    {
                        GlLogger.Instance.Error(ex.Source + "\n" + ex.Message + "\n" + ex.StackTrace);
                    }
                }

            }
        }

        public void addEventListener(EventType type, Action<object, DrawObject, EventArgs> callback)
        {
            if (callback == null)
                return;
            GlEvent evt = new GlEvent(type, this);
            evt.callback = callback;
            List<GlEvent> evts = null;
            if (!_events.TryGetValue(type, out evts))
            {
                evts = new List<GlEvent>();
                _events[type] = evts;
            }
            evts.Add(evt);
            Dictionary<string, HashSet<Action<object, DrawObject, EventArgs>>> dict;          
            if (!_eventCount.TryGetValue(type, out dict))
            {
                dict = new Dictionary<string, HashSet<Action<object, DrawObject, EventArgs>>>();
                _eventCount[type] = dict;
            }
            HashSet<Action<object, DrawObject, EventArgs>> set;
            if (!dict.TryGetValue(BaseID, out set))
            {
                set = new HashSet<Action<object, DrawObject, EventArgs>>();
                dict[BaseID] = set;
            }
            set.Add(callback);            
        }

        public void removeEventListener(EventType type, Action<object, DrawObject, EventArgs> callback)
        {
             if (callback == null)
                return;
             List<GlEvent> evts = null;
             if (_events.TryGetValue(type, out evts))
             {
                 for (int i = 0; i < evts.Count; i++)
                 {
                    if (evts[i].callback == callback)
                    {
                        evts.RemoveAt(i);
                        Dictionary<string, HashSet<Action<object, DrawObject, EventArgs>>> dict;
                        HashSet<Action<object, DrawObject, EventArgs>> set;
                        if (_eventCount.TryGetValue(type, out dict) &&
                            dict.TryGetValue(BaseID, out set))
                        {
                            set.Remove(callback);
                        }                        
                        break;
                    }
                 }
             }     
        }

    }
}
