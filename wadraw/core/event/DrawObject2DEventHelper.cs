﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace wadraw
{
    public class DrawObject2DEventHelper
    {
        private Canvas _canvas;

        private List<DrawObject> tmpCollect;

        private bool _beStart;
        public bool beStart 
        { 
            get
            {
                return _beStart;
            }
            set
            {
                _beStart = value;
                if (_beStart)
                    start();
                else
                    stop();
            }
        }

        public DrawObject2DEventHelper(Canvas canvas)
        {
            tmpCollect = new List<DrawObject>();
            _canvas = canvas;
            beStart = false;
        }

        private void start()
        {
            _canvas.OnCanvasMouseDown += OnMouseDown;
            _canvas.OnCanvasMouseUp += OnMouseUp;
            _canvas.OnCanvasMouseHover += OnMouseHover;
            _canvas.OnCanvasMouseLeave += OnMouseLeave;
            _canvas.OnCanvasMouseEnter += OnMouseEnter;
            _canvas.OnCanvasMouseMove += OnMouseMove;
            _canvas.OnCanvasKeyDown += OnKeyDown;
            _canvas.OnCanvasKeyUp += OnKeyUp;
            _canvas.OnUpdate += OnUpdate;            
        }

        private void stop()
        {
            _canvas.OnCanvasMouseDown -= OnMouseDown;
            _canvas.OnCanvasMouseUp -= OnMouseUp;
            _canvas.OnCanvasMouseHover -= OnMouseHover;
            _canvas.OnCanvasMouseLeave -= OnMouseLeave;
            _canvas.OnCanvasMouseEnter -= OnMouseEnter;
            _canvas.OnCanvasMouseMove -= OnMouseMove;
            _canvas.OnCanvasKeyDown -= OnKeyDown;
            _canvas.OnCanvasKeyUp -= OnKeyUp;
            _canvas.OnUpdate -= OnUpdate;            
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            _emitEventAllObjects(EventType.CanvasKeyUp, sender, e);
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            _emitEventAllObjects(EventType.CanvasKeyDown, sender, e);
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            _emitEventAllObjects(EventType.CanvasMouseMove, sender, e);
        }

        private void OnMouseEnter(object sender, EventArgs e)
        {
            _emitEventAllObjects(EventType.CanvasMouseEnter, sender, e);
        }

        private void OnMouseLeave(object sender, EventArgs e)
        {
            _emitEventAllObjects(EventType.CanvasMouseLeave, sender, e);
        }

        private void OnMouseHover(object sender, EventArgs e)
        {
            _emitEventAllObjects(EventType.CanvasMouseHover, sender, e);
        }

        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            if (GlEventListener.CountEvents(EventType.MouseUp) > 0)
            {
                DrawObject maxZObj = _GetMaxZObj(_GetObjectsAtPoint(e.X, e.Y));
                if (maxZObj != null)
                    maxZObj.Emit(sender, maxZObj, EventType.MouseUp, e);
            }
            _emitEventAllObjects(EventType.CanvasMouseUp, sender, e);
        }

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            if (GlEventListener.CountEvents(EventType.MouseDown) > 0)
            {
                DrawObject maxZObj = _GetMaxZObj(_GetObjectsAtPoint(e.X, e.Y));
                if (maxZObj != null)
                {
                    maxZObj.Emit(sender, maxZObj, EventType.MouseDown, e);
                }
            }
            _emitEventAllObjects(EventType.CanvasMouseDown, sender, e);
        }

        private void OnUpdate(float deltaTime)
        {
            if (GlEventListener.CountEvents(EventType.MouseHover) > 0 ||
                GlEventListener.CountEvents(EventType.MouseLeave) > 0)
            {
                List<DrawObject> drawObject = _canvas._drawObjects;
                DrawObject[] hoverObjs = _GetObjectsAtPoint();
                DrawObject maxZObj = _GetMaxZObj(hoverObjs);
                for (int i = 0; i < drawObject.Count; i++)
                {
                    DrawObject2D obj = drawObject[i] as DrawObject2D;
                    if (obj == null)
                        continue;

                    int index = Array.IndexOf(hoverObjs, drawObject[i]);
                    if (index >= 0 && !obj.beHover && obj.ID == maxZObj.ID) // only top object trigger events...
                    {
                        obj.Emit(null, obj, EventType.MouseHover, new EventArgs());
                        obj.beHover = true;
                    }
                    if ((index < 0 || obj.ID != maxZObj.ID) && obj.beHover)
                    {
                        obj.Emit(null, obj, EventType.MouseLeave, new EventArgs());
                        obj.beHover = false;
                    }
                }
            }
        }

        private void _emitEventAllObjects(EventType type, object sender, EventArgs e)
        {
            List<DrawObject> drawObject = _canvas._drawObjects;
            if (GlEventListener.CountEvents(type) > 0)
            {
                for (int i = 0; i < drawObject.Count; i++)
                {
                    drawObject[i].Emit(sender, drawObject[i], type, e);
                }
            }
        }

        private DrawObject _GetMaxZObj(DrawObject[] coll)
        {
            DrawObject maxZObj = null;
            for (int i = 0; i < coll.Length; i++)
            {
                if (maxZObj == null)
                    maxZObj = coll[i];
                else
                {
                    if (coll[i].z > maxZObj.z)
                        maxZObj = coll[i];
                }
            }
            return maxZObj;
        }

        public DrawObject GetObjectAtPoint(float x, float y)
        {
            return _GetMaxZObj(_GetObjectsAtPoint(x, y));
        }

        public DrawObject[] GetObjectsAtPoint(float x, float y)
        {
            return _GetObjectsAtPoint(x, y);
        }

        public DrawObject[] GetObjectsInRange(Vector4 range)
        {
            List<DrawObject> drawObject = _canvas._drawObjects;
            for (var i = 0; i < drawObject.Count; i++)
            {
                DrawObject2D obj = drawObject[i] as DrawObject2D;

                if (obj != null && obj.visible && obj.beRender) // only visible object can trigger events...
                {
                    if (Util.IntersectionDrawObject2D(obj, range))
                    {
                        tmpCollect.Add(drawObject[i]);
                    }
                }
                else
                {
                    GlLogger.Instance.Error("Convert objcet DrawObject2D failed. i=", i);
                }
            }
            DrawObject[] arr = tmpCollect.ToArray();
            tmpCollect.Clear();
            return arr;
        }

        private DrawObject[] _GetObjectsAtPoint(float x = -1, float y = -1)
        {
            List<DrawObject> drawObject = _canvas._drawObjects;
            if (x < 0 || y < 0)
            {
                x = Util.CursorPos().x;
                y = Util.CursorPos().y;
            }
            for (var i = 0; i < drawObject.Count; i++)
            {
                DrawObject2D obj = drawObject[i] as DrawObject2D;

                if (obj != null && obj.visible && obj.beRender) // only visible object can trigger events...
                {
                    if (Util.IntersectionDrawObject2D(obj, x, y))
                    {
                        tmpCollect.Add(drawObject[i]);
                    }
                }
                else
                {
                    GlLogger.Instance.Error("Convert objcet DrawObject2D failed. i=", i);
                }
            }
            DrawObject[] arr = tmpCollect.ToArray();
            tmpCollect.Clear();
            return arr;
        }
    }
}
