
uniform float uThickness;

varying vec4 gl_Color; // readable on thefragment shader
varying vec3 vBezierCoord;

void main()
{	
	//gl_FragColor = gl_Color;
	vec3 px = dFdx(vBezierCoord);
	vec3 py = dFdy(vBezierCoord);

	float fx = 3.0 * vBezierCoord.x * vBezierCoord.x * px.x - 
                     vBezierCoord.z * px.y - 
	                 vBezierCoord.y * px.z;
	float fy = 3.0 * vBezierCoord.x * vBezierCoord.x * py.x - 
                     vBezierCoord.z * py.y - 
	                 vBezierCoord.y * py.z;

	float sd = (vBezierCoord.x * vBezierCoord.x * vBezierCoord.x - vBezierCoord.y*vBezierCoord.z) / sqrt(fx * fx + fy * fy);
	gl_FragColor = vec4(gl_Color.r, gl_Color.g, gl_Color.b, clamp(1.0-abs(sd), 0.0, 1.0));
	/*
    float diff = clamp(0.5-sd, 0.0, 1.0);
	if (diff == 0.0)
		gl_FragColor = vec4(gl_Color.r, gl_Color.g, gl_Color.b, 0.1);
	else
		gl_FragColor = vec4(gl_Color.r, gl_Color.g, gl_Color.b, 1);
		*/
}