uniform mat4 gl_ModelViewMatrix; // opengl state
uniform mat4 gl_ProjectionMatrix; // opengl state
uniform float uThickness;
uniform float fillColor;
attribute vec4 gl_Vertex; // opengl state
attribute vec4 gl_Color;
attribute vec2 aBezierCoord;

varying vec4 gl_FrontColor; // writable onthe vertex shader
varying vec4 gl_BackColor; // writable onthe vertex shader
varying vec2 vBezierCoord;

void main(void)
{
  vBezierCoord = aBezierCoord;
  gl_FrontColor = gl_Color;
  gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;  
}