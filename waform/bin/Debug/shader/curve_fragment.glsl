
uniform float uThickness;
uniform float fillColor;
varying vec4 gl_Color; // readable on thefragment shader
varying vec2 vBezierCoord;

void main()
{
	//gl_FragColor = gl_Color;
	vec2 px = dFdx(vBezierCoord);
	vec2 py = dFdy(vBezierCoord);
	float fx = 2.0 * vBezierCoord.x * px.x - px.y;
	float fy = 2.0 * vBezierCoord.y * py.x - py.y;

	// signed distance
	float sd = (vBezierCoord.x * vBezierCoord.x - vBezierCoord.y) / sqrt(fx * fx + fy * fy);

	if (fillColor == 1.0)
		gl_FragColor = vec4(gl_Color.r, gl_Color.g, gl_Color.b, clamp(uThickness-sd, 0.0, 1.0));
	else
		gl_FragColor = vec4(gl_Color.r, gl_Color.g, gl_Color.b, clamp(uThickness-abs(sd), 0.0, 1.0));
		
	//gl_FragColor = vec4(gl_Color.r, gl_Color.g, gl_Color.b, 1);
}