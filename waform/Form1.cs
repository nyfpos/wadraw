﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using wadraw;

namespace waform
{
    public partial class Form1 : Form
    {
        private Canvas _canvas;

        public Form1()
        {
            InitializeComponent();

            _canvas = new Canvas(glControl1);
            _canvas.frameRate = 1000.0f / 60.0f;
            _canvas.beStart2DEvent = true;
            _canvas.beSelect = true;
            _canvas.autoArrangeZByAddedOrder = true;
            _canvas.OnCanvasLoaded += glControl1_Load;
        }

        private float angle = 0.0f;
        /*
        private void _draw2DSelectionTest()
        {
            Quad obj = new Quad(30.0f);
            _canvas.addObject(obj);

            obj.x = (float)_canvas.width * 0.5f;
            obj.y = (float)_canvas.height * 0.5f;
            obj.color = 0x3EF5A3;//0x20D080;
            obj.fillColor = false;
            obj.name = "obj_1";
            obj.visible = false;

            bool beDown = false;
            wadraw.Point p1 = new wadraw.Point(), p2 = new wadraw.Point();
            _canvas.OnCanvasMouseDown += (sender, e) =>
            {
                MouseEventArgs me = e as MouseEventArgs;
                p1 = new wadraw.Point(me.X, me.Y);
                beDown = true;
                obj.visible = true;
                obj.x = me.X;
                obj.y = me.Y;
                obj.width = 1.0f;
                obj.height = 1.0f;
            };

            _canvas.OnCanvasMouseUp += (sender, e) =>
            {
                obj.visible = false;
                beDown = false;
            };

            _canvas.OnCanvasMouseMove += (sender, e) =>
            {
                MouseEventArgs me = e as MouseEventArgs;
                if (beDown)
                {
                    p2 = new wadraw.Point(me.X, me.Y);
                    obj.reset(p1, p2);
                    //GlLogger.Instance.LogInfo(string.Format("({0},{1}) ({2},{3})", p1.x, p1.y, p2.x, p2.y));
                    //GlLogger.Instance.LogInfo(string.Format("({0},{1}) ({2},{3})", obj.x, obj.y, obj.width, obj.height));
                }
            };
        }
        */

        private void _draw2DTest2()
        {
            _canvas.antiAliased = true;
            _canvas.color = 0x333333;
            GlDeserializer.Instance.Deserialize(System.IO.File.ReadAllText(@"D:\canvas_test.json"));
        }


        private void _draw2DTest()
        {
            GlLogger.Instance.BindLogInfo(LogInfo);
       //   _canvas.drawObject2DSelectHelper.enable = false;
            /*
            _canvas.drawObject2DSelectHelper.mode = SelectHelperMode.FrameOnly;
            _canvas.drawObject2DSelectHelper.frameLineType = LineType.DashedLine;
            _canvas.drawObject2DSelectHelper.color = 0x00FF00;
            _canvas.drawObject2DSelectHelper.offsetXY = new OpenTK.Vector2(2.0f, 2.0f);
            _canvas.drawObject2DSelectHelper.border = 1.5f;
            _canvas.drawObject2DSelectHelper.mode = SelectHelperMode.Normal;
            */
           // _canvas.updateMode = CanvasUpdateMode.Normal;
           _canvas.antiAliased = true;
           _canvas.color = 0x333333;//0x6090C0;// 0x333333;           
          // _canvas.zoom = 0.5f;
          // _canvas.zoomXY = new OpenTK.Vector2(50, -50);
            /*
           DrawImage img2 = new DrawImage(@"gauge-md.png");
           _canvas.addObject(img2);
            img2.x = img2.y = 0.0f;
             */
            
            
           DrawImage img = new DrawImage(@"./logo-ps.png");
           img.x = 0;
           img.y = 0;
           _canvas.addObject(img);
            
            /*

           PolyLine pline = new PolyLine();
           _canvas.addObject(pline);
           pline.addPoint(new wadraw.Point(200, 200));
           pline.addPoint(new wadraw.Point(250, 200));           
           pline.addPoint(new wadraw.Point(250, 250));
           pline.addPoint(new wadraw.Point(200, 250));
           pline.addPoint(new wadraw.Point(200, 200));*/
            /*
           Bitmap bit = new Bitmap(@"./logo-ps.png");
           DrawImage img = new DrawImage("");
           img.LoadTexture(bit);
           _canvas.addObject(img);
            */


        //   img.beDrag = true;
            
           Triangle trangle = new Triangle();
           _canvas.addObject(trangle);
           trangle.reset(100.0f, 100.0f, 150.0f, 150.0f);
           trangle.fillColor = true;            
          
           Line line = new Line();           
           line.x = 10.0f;
           line.y = 10.0f;           
           line.reset(250.0f, 250.0f, 280.0f, 250.0f);
           line.visible = false;
                    
           Line line2 = (Line)line.Clone();           
           _canvas.addObject(line2);
            //  line2.reset(250, 250, 250, 200);
           line2.color = 0xFF0000;
           
           line2.reset(250, 250, 200, 250);
//           line2.rotateAroundZ(65);
           line2.border = 2.0f;
           line2.lineType = LineType.DashedDotedLine;
           LogInfo_Replace("line height:", line2.height, " width:", line2.width);
            //line2.visible = false;
            //LogInfo_Replace(line2.toJsonString());
           /*
                    //Line line4 = GlDeserializer.Instance.toDrawObject2D(line2.toJsonString()) as Line;
                    Line line4 = line2.Clone() as Line;
                    line4.color = 0xFF0000;
                    line4.reset(250.0f, 250.0f, 280.0f, 250.0f);
                     _canvas.addObject(line4);
                     */
            /*
            Circle circle = new Circle();
            circle.color = 0xFF0000;
           _canvas.addObject(circle);           
           circle.reset(250, 250, 50);


           Curve curve = new Curve();
           curve.fillColor = false;
           curve.thickness = 1.5f;
           curve.width = 100;
           curve.height = 100;
           curve.lineType = LineType.DashedLine;
           _canvas.addObject(curve);

            */
            
           DrawText txt = new DrawText();           
           txt.name = "DrawText_hello";
           txt.backgroundColor = _canvas.color;
           txt.fontColor = 0xFF0000;
           txt.fontFamily = FontFamily.GenericSansSerif;           
           txt.x = 100.0f;
           txt.y = 100.0f;
           txt.fontSize = 18;
           txt.text = "我是 John!";
           LogInfo_Replace(txt.toJObject().ToString());
           _canvas.addObject(txt);
            
            //   txt.visible = false;
          //  LogInfo_Replace(txt.toJsonString());

        //   circle.visible = false;
        //   circle.reset(0.0f, 0.0f, 50.0f, 50.0f);
        //   circle.fillColor = true;
                      
           // line.leftTop.x
         // GlLogger.Instance.LogInfo("line ({0},{1}) ({2},{3})", line.leftTop.x, line.leftTop.y, line.rightBottom.x, line.rightBottom.y);
    
           // _draw2DSelectionTest();
            /*
           Quad obj = new Quad();           
           _canvas.addObject(obj);
           obj.x = (float)_canvas.width * 0.5f;
           obj.y = (float)_canvas.height * 0.5f;
           obj.color = 0xFFFFFF;
           obj.fillColor = true;
           obj.name = "obj_1";             
      //    obj.pivotType = PivotType.Center;
           obj.rotateAroundZ(45);

           Quad obj2 = new Quad();
           _canvas.addObject(obj2);
           obj2.x = obj.x + 15.0f;
           obj2.y = obj.y + 15.0f;
           obj2.alpha = 0.5f;
           obj2.color = 0xFF0000;
           obj2.name = "obj_2";
           obj2.fillColor = true;
            
           Quad obj3 = (Quad)obj2.Clone();//new Quad(30.0f);            
           _canvas.addObject(obj3);            
           obj3.x += 15.0f ;
           obj3.y += 15.0f ;
           obj3.alpha = 0.9f;
           obj3.name = "obj_3";
           obj3.color = 0x0000FF;
           obj3.alpha = 1.0f;


            
            obj.addEventListener(EventType.MouseDown, _PrintMouseDown);
            obj.addEventListener(EventType.MouseUp, _PrintMouseUp);
            obj2.addEventListener(EventType.MouseDown, _PrintMouseDown);
            obj2.addEventListener(EventType.MouseUp, _PrintMouseUp);
            */


            //obj.beDrag = true;
  

         //   LogInfo_Replace("object width,height:", obj.width, obj.height);/*, "\r\nobject leftTop:", obj.leftTop.x, obj.leftTop.y, "rightBottom:", obj.rightBottom.x, obj.rightBottom.y);
         //   obj.rotateAroundZ(354.7726f);
         //   LogInfo_Replace("object width,height:", obj.width, obj.height, "\r\nobject leftTop:", obj.leftTop.x, obj.leftTop.y, "rightBottom:", obj.rightBottom.x, obj.rightBottom.y);
            /*
            obj.visible = obj2.visible = obj3.visible = false;

            bool[] triggered = new bool[]
            {
                false,
                false
            };

            Random rand = new Random();
            int MaxCnt = 512 + (int)(rand.NextDouble() * 512.0f);
           */
          // _canvas.color = 0xFF0000;
           //System.IO.File.WriteAllText(@"D:\canvas_test.json", GlSerializer.Instance.Serialize());
          // GlDeserializer.Instance.Deserialize(System.IO.File.ReadAllText(@"D:\canvas_test.json"));
/*
           bool beSet = false, beClr = false;
           float timeAcc = 0.0f;*/
            int cnt = 0;
            wadraw.Point org = new wadraw.Point(250.0f, 250.0f);
            float r = 50.0f;
            bool beNextFrame = false;
            _canvas.OnUpdate += (deltaTime) =>
            {/*
                if (!beNextFrame)
                    beNextFrame = true;
                else
                    line2.color = 0x00FF00;
                */

          //      LogInfo_Replace(string.Format("group_XY:{0},{1} group_WH:{2},{3}", group.x, group.y, group.width, group.height));
             //   LogInfo_Replace(string.Format("{0},{1}", group.x, group.y));
               // LogInfo_Replace("deltaTime: " + deltaTime);
             // LogInfo_Replace("object width,height:", obj.width, obj.height, "\r\nobject leftTop:", obj.leftTop.x, obj.leftTop.y, "rightBottom:", obj.rightBottom.x, obj.rightBottom.y);
              //  LogInfo_Replace("object3 width,height:", obj3.width, obj3.height, "\r\nobject leftTop:", obj3.leftTop.x, obj3.leftTop.y, "rightBottom:", obj3.rightBottom.x, obj3.rightBottom.y);
               const int speed = 10;
               angle += (deltaTime);
                /*
               timeAcc += deltaTime;
               if (timeAcc > 10.0f && !beSet)
               {                 
                   pline.addPoint(new wadraw.Point(280, 250));
                   beSet = true;
               }

               if (timeAcc > 20.0f && !beClr)
               {
                   pline.clear();
                   beClr = true;
               }*/

            //    if (_canvas.zoom <2.0f)
            //   _canvas.zoom +=deltaTime;

           //    obj.rotateAroundZ(angle);
          //     obj.x += 1;
          //     obj.y += 1;

               //obj.y += 1;              
               //obj.rotateAroundZ(angle);   
               //obj.scaleX(angle);
               //obj.scaleY(angle);                                      
               
               //if (obj.x < 468)
               //{
               //    obj.x += 1;
               //    obj.y += 1;
               //}
                
                
               // if (obj3.x < 468)
               // {
               //     obj3.x += 1;
               //     obj3.y += 1;
               // }
                
                // obj.transform.rotation = obj.transform.rotation * Gut.ToQ(0, 0, 500 * deltaTime);                
                label3.Text = Util.CursorPos().x.ToString() + "," + Util.CursorPos().y.ToString();
                label6.Text = _canvas.fps.ToString();
          //      label4.Text = obj.x.ToString() + "," + obj.y.ToString();
            };
            
        }

        private void _PrintMouseDown(object sender, DrawObject dwobj, EventArgs e)
        {
            MouseEventArgs me = e as MouseEventArgs;
            LogInfo(string.Format("{0}.MouseDown:", dwobj.name), me.X, me.Y);            
        }

        private void _PrintMouseUp(object sender, DrawObject dwobj, EventArgs e)
        {
            MouseEventArgs me = e as MouseEventArgs;
            LogInfo(string.Format("{0}.MouseUp:", dwobj.name), me.X, me.Y);            
        }

        private void _draw3DTest()
        {
            _canvas.beBlend = false;
            Sphere obj = new Sphere(3.0f);
            _canvas.addObject(obj);            
//            obj.fillColor = true;
            _canvas.OnUpdate += (deltaTime) =>
            {
                obj.transform.rotation = obj.transform.rotation * Gut.ToQ(0, 10 * deltaTime, 0);
            };
        }
          
        private void glControl1_Load(object sender, EventArgs e)
        {
            label8.Text = _canvas.width.ToString() + "," + _canvas.height.ToString();
            _draw2DTest();
            //_draw2DTest2();
        }

        //===================================
        private void LogInfo_Replace(params object[] args)
        {
            string str = "", glue = "";
            for (int i = 0; i < args.Length; i++)
            {
                str = str + glue + args[i].ToString();
                glue = " ";
            }
            str += Environment.NewLine;
            textBox1.Text = str;
        }

        private void LogInfo(params object[] args)
        {
            string str = "", glue="";
            for (int i = 0; i < args.Length; i++)
            {
                str = str + glue + args[i].ToString();
                glue = " ";
            }
            str += Environment.NewLine;
            textBox1.Text += str;
        }


    }
}
